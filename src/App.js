import React, { Component } from 'react'
import Dashboard from './modules/dashboard/Dashboard.js'
import Projects from './modules/projects/Projects.js'
import ProjectDetails from './modules/projects/ProjectDetails.js'
import Clients from './modules/clients/Clients.js'
import Brands from './modules/brands/Brands.js'
import ActivityTypes from './modules/activityTypes/ActivityTypes.js'
import Users from './modules/users/Users.js'
import Login from './modules/login/Login.js'
import Bootstrap from './modules/ui/Bootstrap.js'
import Logout from './modules/logout/Logout.js'
import './App.scss'
import { Route } from 'react-router-dom'
import KanbanDesk from './modules/kanbanDesk/KanbanDesk.js'
import ClientUnits from './modules/clientUnits/ClientUnits';
import IdgUnits from './modules/idgUnits/IdgUnits';
import Timesheet from './modules/timesheet/Timesheet';
import Income from './modules/reports/Income';
import MissingPOs from './modules/reports/MissingPOs';
import MissingTimesheet from './modules/reports/MissingTimesheet';
import Occupancy from './modules/reports/Occupancy';
import LoadingByActivityType from './modules/reports/LoadingByActivityType';
import TimesheetReport from './modules/reports/Timesheet';
import Overtime from './modules/reports/Overtime';
import { ToastContainer } from 'react-toastify';

class App extends Component {
    
    componentDidMount(){
        document.title = "Timesheet - 2.0"
    }

    render() {
        return (
            <div className="App">
                <ToastContainer autoClose={3000} />
                <Route exact path="/" render={() => (<Dashboard/>)}/>
                <Route exact path="/projects" render={() => (<Projects/>)}/>
                <Route path="/project/:projectId" render={() => (<ProjectDetails/>)}/>
                <Route exact path="/login" render={() => (<Login/>)}/>
                <Route exact path="/timesheet" render={() => (<Timesheet/>)}/>
                <Route exact path="/kanban" render={() => (<KanbanDesk/>)}/>

                {/* Configuration routes */}
                <Route exact path="/config/clients" render={() => (<Clients/>)}/>
                <Route exact path="/config/client-units" render={() => (<ClientUnits/>)}/>
                <Route exact path="/config/idg-units" render={() => (<IdgUnits/>)}/>
                <Route exact path="/config/brands" render={() => (<Brands/>)}/>
                <Route exact path="/config/activity-types" render={() => (<ActivityTypes/>)}/>
                <Route exact path="/config/hogarth-pms" render={() => (<Users roleId="3"/>)}/>
                <Route exact path="/config/idg-pms" render={() => (<Users roleId="2"/>)}/>
                <Route exact path="/config/developers" render={() => (<Users roleId="4"/>)}/>

                {/* Reports routes */}
                <Route exact path="/reports/income" render={() => (<Income/>)}/>
                <Route exact path="/reports/missing-pos" render={() => (<MissingPOs/>)}/>
                <Route exact path="/reports/missing-timesheet" render={() => (<MissingTimesheet/>)}/>
                <Route exact path="/reports/occupancy" render={() => (<Occupancy/>)}/>
                <Route exact path="/reports/loading-activity-type" render={() => (<LoadingByActivityType/>)}/>
                <Route exact path="/reports/timesheet" render={() => (<TimesheetReport/>)}/>
                <Route exact path="/reports/overtime" render={() => (<Overtime/>)}/>

                <Route exact path="/ui-bootstrap" render={() => (<Bootstrap/>)}/>

                <Route exact path="/logout" render={() => (<Logout/>)}/>
            </div>
        );
    }
}

export default App;