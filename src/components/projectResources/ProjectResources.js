import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import axios from 'axios';
import TableRow from "../../components/tableRow/TableRow";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import Preloader from "../../components/preloader/Preloader";
import AddResource from "../../components/addResource/AddResource";
import { toast } from 'react-toastify';
import confirm from 'reactstrap-confirm';

class ProjectResources extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            resources: [],
            showAddResource: false
        };
    }

    componentDidMount() {
        this.getProjectResources();
    }

    getProjectResources = () => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project-resources/' + this.props.projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                resources: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    removeProjectResource = async (resourceId) => {
        const { cookies } = this.props;

        let result = await confirm({
            title: 'Are you sure?',
            message: "This action is irreversible",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            this.setState({
                loading: true
            })

            axios.delete(process.env.REACT_APP_API_URL + 'project-resources/' + resourceId, {
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getProjectResources(this.props.projectId);
                toast.success("Resource has been removed.");
            })
            .catch(err => {
                if(err.response.status === 401){
                    //redirect to login
                    this.props.toggleRedirectToLogin(true);
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
        }
    }

    openResourceModal = () => {
        this.setState({
            showAddResource: true
        });
    }
    closeResourceModal = refreshList => {
        this.setState({
            showAddResource: false
        });
        if(refreshList) {
            this.getProjectResources(this.props.projectId);
        }
    }

    render() {

        return (
            <div className="project-subsection">
                { this.state.loading && <Preloader/> }
                <div className="row project-subsection--head">
                    <div className="col-10"><h1>Assigned resources</h1></div>
                    <div className="col-2 ta-right">
                        <UncontrolledDropdown>
                            <DropdownToggle nav>
                                <i className="la la-plus-circle"></i>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem key={'rs-1'} onClick={this.openResourceModal}>New resource</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </div>
                {this.state.showAddResource &&
                    <AddResource
                        projectId={this.props.projectId}
                        closeResourceModal={this.closeResourceModal}
                        showPopup={this.state.showAddResource}
                    />
                }
                <div className="project-subsection--body">
                    {this.state.resources.map((resource, index) => {
                        return(
                            <TableRow
                                key={'pr-' + index}
                                tableCols={
                                    [
                                        {
                                            value: resource.user_name,
                                            label: 'User',
                                            mobileFullWidth: true,
                                            customCssClass: 'row-head-mobile'
                                        },
                                        {
                                            value: resource.activity,
                                            label: 'Activity'
                                        },
                                        {
                                            value: resource.max_hours,
                                            label: 'Hours'
                                        },
                                        {
                                            ddOptions: [
                                                {
                                                    value: 'Remove',
                                                    action: {
                                                        name: 'removeProjectResource',
                                                        params: resource.id
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                                removeProjectResource={this.removeProjectResource}
                            />
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default withCookies(ProjectResources);