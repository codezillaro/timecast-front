import React, { Component } from 'react';
import './Header.scss';
import { withCookies } from 'react-cookie';
import profilePicture from '../../assets/img/profile-pic.png';
import cosminProfile from '../../assets/img/cosmin-doroftei.png';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';

class Header extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            userRole: -1,
            userAvatar: '',
            menuNotifications:
                [
                    {
                        "notificationTitle" : "Lorem ipsum notification",
                        "notificationText": "How many free autoresponders have"
                    },
                    {
                        "notificationTitle" : "Lorem",
                        "notificationText": "Another title for this article can be"
                    },
                    {
                        "notificationTitle" : "Traffic Of Your Blog",
                        "notificationText": "What makes one logo better than"
                    },
                    {
                        "notificationTitle" : "Search Engine Optimization",
                        "notificationText": "Adwords Keyword research"
                    }
                ],

            menuUser:
                [
                    {
                        titleMenuUser: "PROFILE",
                        cssIconClass: "la la-user",
                        href: '/profile'
                    },
                    {
                        titleMenuUser: "SETTINGS",
                        cssIconClass: "la la-cog",
                        href: '/settings'
                    },
                    {
                        titleMenuUser: "SIGN OUT",
                        cssIconClass: "la la-sign-out",
                        href: '/logout'
                    },
                ]
        }
    }

    componentDidMount() {
        const { cookies } = this.props;

        //role = 2 for superadmin or idg pm and 1 for devs
        this.setState({
            userRole: cookies.get('userRole'),
            userAvatar: cookies.get('userRole') == 2 ? profilePicture : cosminProfile
        })

    }

    actionClicked = (functionName, functionParams) => {
        this.props[functionName](functionParams);
    }

    render() {
        return (
            <div className="Header">
                <div className="header-container">
                    
                    <div className="left-menu">
                        {this.props.roleNeeded && this.props.headerOptions && this.state.userRole == this.props.roleNeeded &&
                            <span className="dropdown-icon">
                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        <i className="la la-plus-circle"></i>
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        {this.props.headerOptions.map((action, index) => {
                                            return (
                                                <DropdownItem key={'dd-' + index} onClick={ () => this.actionClicked(action.action, action.params) }>
                                                    <i className={action.iconClass ? action.iconClass : 'la la-suitcase'}></i> {action.name}
                                                </DropdownItem>
                                            )
                                        })}
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </span>
                        }
                        <span className="page--title d-none d-md-inline-block">{this.props.pageTitle}</span>
                    </div>
                
                    <div className="right-side">
                        <div className="search-bar d-none d-md-inline-block">
                            <form className="search-form">
                                <div className="dropdown-icon dropdown-search">
                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle nav caret>
                                            <span>
                                                <input type="text" placeholder="Search for task and etc"/>
                                                <i className="la la-search"></i>
                                            </span>
                                        </DropdownToggle>
                                        <DropdownMenu right>
                                            <DropdownItem href="/xxx">
                                                <i className="la la-suitcase"></i> Search for projects
                                            </DropdownItem>
                                            <DropdownItem href="/xxx">
                                                <i className="la la-list-alt"></i> Search for asigned tasks
                                            </DropdownItem>
                                            <DropdownItem href="/xxx">
                                                <i className="la la-calendar"></i> Search for created events
                                            </DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </div>
                            </form>
                        </div>
                        <ul className="menu-right">
                            <div className="messages-dropdown dropdown-menu-custom ">
                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        <i className="la la-bell"></i>
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        {
                                            this.state.menuNotifications.map((item, index) => {
                                                return (
                                                    <DropdownItem key={'notification-' + index} href="/notification" className="message-box">
                                                        <div className="message-box-items">
                                                            <div className="img-message">
                                                                <img src={this.state.userAvatar} className="user-round-avatar" alt="profile" title=""/>
                                                            </div>
                                                            <div className="text-message">
                                                                <p className="title"> {item.notificationTitle}  </p>
                                                                <p  className="description"> {item.notificationText}</p>
                                                            </div>
                                                        </div>
                                                    </DropdownItem>
                                                )
                                            })
                                        }
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>                   
                            <div className="profile-dropdown dropdown-menu-custom ">
                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        <img src={this.state.userAvatar} alt="profile" title="" className="user-round-avatar" />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        {
                                            this.state.menuUser.map((item, index) => {
                                                return (
                                                    <DropdownItem href={item.href} key={'profile-link-' + index} className="message-box user-box">
                                                        <div className="message-box-items user-box-items">
                                                            <div className="text-message">
                                                                <span className="title"> <i className={item.cssIconClass}></i>{item.titleMenuUser}</span>
                                                            </div>
                                                        </div>
                                                    </DropdownItem>
                                                )
                                            })
                                        }
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default withCookies(Header);
