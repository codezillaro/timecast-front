import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import axios from 'axios';
import TableRow from "../../components/tableRow/TableRow";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import Preloader from "../../components/preloader/Preloader";
import confirm from 'reactstrap-confirm';
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import AddTimesheet from "../../components/addTimesheet/AddTimesheet";

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class TimesheetEntries extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            tsEntries: [],
            showAddTimesheet: false,
            userRole: -1,
            editTEId: -1
        };
    }

    componentDidMount() {
        const { cookies } = this.props;

        this.setState({
            userRole: cookies.get('userRole')
        })

        this.getTimesheetEntries();
    }

    getTimesheetEntries = () => {
        const {cookies} = this.props;

        this.setState({
            loading: true
        });

        axios.get(process.env.REACT_APP_API_URL + 'ts-by-projectId/' + this.props.projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                tsEntries: res.data.data,
                loading: false,
                showAddTimesheet: false,
                editTEId: -1
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    openModal = () => {
        this.setState({
            showAddTimesheet: true
        });
    }

    closeModal = refreshList => {
        this.setState({
            showAddTimesheet: false,
            editTEId: -1
        });
        if(refreshList) {
            this.getTimesheetEntries();
        }
    }

    editTSEntry = (teId) => {
        this.setState({
            editTEId: teId,
            showAddTimesheet: true
        })
    }

    deleteTSEntry = async (teId) => {
        const { cookies } = this.props;
        let result = await confirm({
            title: 'Are you sure?',
            message: "This action is irreversible",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            this.setState({
                loading: true
            })

            axios.delete(process.env.REACT_APP_API_URL + 'ts/' + teId, {
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getTimesheetEntries();
                toast.success("Timesheet entry has been deleted.");
            })
            .catch(err => {
                if(err.response.status === 401){
                    //redirect to login
                    this.props.toggleRedirectToLogin(true);
                } else {
                    this.setState({
                        loading: false
                    })
                }
                toast.error("An error occured, please try again later.");
            })
        }
    }

    render() {
        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="project-subsection">
                { this.state.loading && <Preloader/> }
                <div className="row project-subsection--head">
                    <div className="col-10"><h1>Timesheet entries</h1></div>
                    <div className="col-2 ta-right">
                        <UncontrolledDropdown>
                            <DropdownToggle nav>
                                <i className="la la-plus-circle"></i>
                            </DropdownToggle>
                            <DropdownMenu right>
                                {this.state.userRole == 1 &&
                                    <DropdownItem onClick={(e) => { this.openModal() }} key={'te-1'}>New entry</DropdownItem>
                                }
                                {this.state.userRole == 2 &&
                                    <DropdownItem key={'te-2'}>Export CSV</DropdownItem>
                                }

                                {this.state.showAddTimesheet &&
                                    <AddTimesheet
                                        teId={this.state.editTEId != -1 ? this.state.editTEId : ''}
                                        closeModal={this.closeModal}
                                        showPopup={this.state.showAddTimesheet}
                                    />
                                }
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </div>    

                <div className="project-subsection--body">
                {
                    this.state.tsEntries.map((item, index) => {
                        return (
                            <TableRow
                                key = {'row-' + index}
                                tableCols = {
                                    [
                                        {
                                            value: this.state.userRole == 2 ? item.user : '',
                                            label: this.state.userRole == 2 ? 'User' : ''
                                        },
                                        {
                                            value: item.manual_date,
                                            dateFormat: 'DD.MM.YYYY', //momentjs formats
                                            label: 'Date'
                                        },
                                        {
                                            value: item.hours,
                                            label: 'Hours'
                                        },
                                        {
                                            value: item.activity_name,
                                            label: 'Activity',
                                            customCssClass: 'row-details'
                                        },
                                        {
                                            value: item.extra_request,
                                            label: 'Extra',
                                            showAsCheckbox: true
                                        },
                                        {
                                            value: item.qa_issue,
                                            label: 'QA',
                                            showAsCheckbox: true
                                        },
                                        {
                                            value: item.overtime,
                                            label: 'OT',
                                            showAsCheckbox: true
                                        },
                                        {
                                            value: item.details,
                                            label: 'Details',
                                            customCssClass: 'row-details-3'
                                        },
                                        this.state.userRole == 1 ? {
                                            ddOptions: [
                                                {
                                                    value: 'Edit entry',
                                                    action: {
                                                        name: 'editTSEntry',
                                                        params: item.id
                                                    }
                                                },
                                                {
                                                    value: 'Remove entry',
                                                    action: {
                                                        name: 'deleteTSEntry',
                                                        params: item.id
                                                    }
                                                }
                                            ]
                                        } : {}
                                    ]
                                }
                                editTSEntry={this.editTSEntry}
                                deleteTSEntry={this.deleteTSEntry}
                            />
                        )
                    })
                }
                </div>
            </div>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withCookies(TimesheetEntries));
