import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader, Form, FormGroup, Label, Input, Col, Row, ListGroup, ListGroupItem } from 'reactstrap';
import './AddPopup.scss';
import { withCookies } from 'react-cookie';
import profilePicture from '../../assets/img/profile-pic.png'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class AddPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            startDate: new Date()
        };
        
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    render() {
        return (
            <div>
                <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel} <i className="la la-plus"></i>New task</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} wrapClassName="component--modal-new-project component--modal-new-task" className={this.props.className}>

                    <ModalHeader toggle={this.toggle}>New task</ModalHeader>
                    <ModalBody className="new--project">
                        <Form>
                            <Row form>

                                <Col md={8}>
                                    <FormGroup className="name-your-task--ts">
                                        <i className="la la-check-square"></i>
                                        <Input className="name-your-task-text--ts" placeholder="Name your task" />
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="assigned-new--task--ts">Assigned to</div>
                                        <Button className="btn--ts btn--ts-primary button-assigned-new--task--ts" id="button-assigned-new--task--ts" onClick={this.toggle}>{this.props.buttonLabel} <i className="la la-plus"></i></Button>
                                    </FormGroup>

                                    <FormGroup className="new-task--description">
                                        <Label for="new-task--description" ><i className="la la-align-left"></i><span className="new-task--titles--ts">Description</span></Label>
                                        <Input type="textarea" name="text" className="textarea--ts" id="new-task--description" placeholder="Add your description" />
                                    </FormGroup>

                                    <FormGroup>
                                        <Label for="add-popup--filepath"><i className="la la-link"></i> <span className="new-task--titles--ts">Attachments</span> </Label>
                                        <Label for="add-popup--filepath" className="add-popup--upload-attach"><i className="la la-plus-circle"></i><span className="upload-new-task--ts">UPLOAD ATTACHMENTS</span></Label>
                                        <Input type="file" name="file" id="add-popup--filepath" className="add-popup--filepath" />
                                    </FormGroup>

                                    <FormGroup>
                                        <Label for="new-task--comments" ><i className="la la-comments"></i><span className="new-task--titles--ts">Comments</span></Label>
                                        <Input type="textarea" cols="2" rows="10" name="text" className="textarea--ts new-task--comments" id="new-task--comments" placeholder="Your message" />
                                        <img src={profilePicture} alt="img" title="user's name" />
                                        <Button color="primary" className="btn--ts-grey btn--ts" onClick={this.toggle}>ADD</Button>{' '}
                                    </FormGroup>

                                </Col>

                                {/* Actions column */}

                                <Col md={4}>
                                    <FormGroup>
                                        <div className="actions--new-task--ts">Actions</div>
                                    </FormGroup>

                                    <ListGroup>
                                        <Label className="duedate-new-task--ts"><i className="la la-calendar"></i><span className="actions-buttons--new-task--ts">DUE</span>
                                            <DatePicker 
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                className="calendar-new-task--ts"                                                
                                            />
                                        </Label>
                                        <ListGroupItem tag="a" href="#"><i className="la la-check"></i><span className="actions-buttons--new-task--ts">MARK AS DONE</span></ListGroupItem>
                                        <ListGroupItem tag="a" href="#"><i className="la la-clone"></i><span className="actions-buttons--new-task--ts">DUPLICATE</span></ListGroupItem>
                                        <ListGroupItem tag="a" href="#"><i className="la la-link"></i><span className="actions-buttons--new-task--ts">ADD ATTACHMENT</span></ListGroupItem>
                                        <ListGroupItem tag="a" href="#"><i className="la la-user"></i><span className="actions-buttons--new-task--ts">ADD MEMBER</span></ListGroupItem>
                                        <ListGroupItem tag="a" href="#"><i className="la la-volume-up"></i><span className="actions-buttons--new-task--ts">NOTIFICATIONS</span></ListGroupItem>
                                        <ListGroupItem tag="a" href="#"><i className="la la-trash"></i><span className="actions-buttons--new-task--ts">DELETE</span></ListGroupItem>
                                    </ListGroup>

                                </Col>

                            </Row>
                        </Form>
                    </ModalBody>

                </Modal>
            </div>
        );
    }
}

export default withCookies(AddPopup);