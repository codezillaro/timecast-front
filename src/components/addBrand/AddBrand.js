import React, { Component } from 'react';
import { Modal, ModalBody, ModalFooter, ModalHeader, FormGroup } from 'reactstrap';
import { withCookies } from 'react-cookie';
import { Formik, Form, ErrorMessage } from 'formik';

class AddBrand extends Component {

    render() {
        return (
            <div>
                <Modal isOpen={this.props.showPopup} centered wrapClassName="component--modal-new-project">
                    <Formik
                        initialValues = {{ brandId: this.props.brandId, brandName: this.props.brandName, hogarthVisible: this.props.hogarthVisible }}
                        validate={values => {
                            let errors = {};
                            if (!values.brandName) {
                                errors.brandName = 'Required';
                            }
                            return errors;
                        }}
                        onSubmit={(values, { setSubmitting }) => {
                            this.props.saveBrand(values);
                            setSubmitting(false);
                        }}
                        >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            /* and other goodies */
                        }) => (
                            <Form>
                                <ModalHeader>Add new brand</ModalHeader>
                                <ModalBody className="new--project">
                                    <input type="hidden" name="brandId" value={values.brandId}/>
                                    <FormGroup>
                                        <label htmlFor="brand--name" className="text-left--ts">Name</label>
                                        <input
                                            type="text"
                                            name="brandName"
                                            id="brand--name"
                                            value={values.brandName}
                                            onChange={handleChange}
                                            className={errors.brandName ? 'form-control error' : 'form-control'}
                                            placeholder="Name"
                                        />
                                        <ErrorMessage name="brandName" className="form-error" component="div" />
                                    </FormGroup>

                                    <FormGroup>
                                        <label className="chk-container">Hogarth visible
                                            <input type="checkbox" name="hogarthVisible" checked={values.hogarthVisible} onChange={handleChange} id="brand--hogarth-visible" />
                                            <span className="checkmark"></span>
                                        </label>
                                    </FormGroup>    
                                
                                </ModalBody>
                                <ModalFooter>
                                    <button type="submit" disabled={isSubmitting} className="btn--ts-primary btn--ts">{isSubmitting ? 'SAVING ...' : 'SAVE BRAND'}</button>
                                    <button type="button" className="btn--ts-grey btn--ts" onClick={this.props.closeModal}>CANCEL</button>
                                </ModalFooter>
                            </Form>
                        )}
                    </Formik>
                </Modal>
            </div>
        );
    }
}

export default withCookies(AddBrand);