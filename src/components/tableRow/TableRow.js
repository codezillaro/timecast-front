import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import './TableRow.scss';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

class TableRow extends Component {

    actionClicked = (functionName, functionParams) => {
      this.props[functionName](functionParams);
    }

    render() {
        return (
            <div className="TableRow row justify-content-between" key={this.props.key}>
                <div className="col">
                    <div className="row align-items-center">
                        {
                            this.props.tableCols && this.props.tableCols.map((col, index) => {

                                let colClassName = 'col ';
                                colClassName += col.hiddenMobile ? 'd-none d-sm-block ' : '';
                                colClassName += col.mobileFullWidth ? 'col-12 col-sm ' : '';
                                colClassName += col.customCssClass ? (col.customCssClass + ' ') : '';

                                return (
                                  col.ddOptions ?
                                    <div className="col dd-col" key={'col-' + index}>
                                        {col.ddOptions && Array.isArray(col.ddOptions) ?
                                            <UncontrolledDropdown>
                                                <DropdownToggle nav>
                                                    <i className='la la-ellipsis-h'></i>
                                                </DropdownToggle>
                                                <DropdownMenu right>
                                                    {
                                                        
                                                            col.ddOptions.map((ddOption, index) => {
                                                                return (
                                                                    <DropdownItem key={'dd-' + index} onClick={ () => this.actionClicked(ddOption.action.name, ddOption.action.params) }>{ddOption.value}</DropdownItem>
                                                                )
                                                            })
                                                        

                                                    }
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        :
                                            <button type="button" className={col.ddOptions.iconClass} onClick={ () => this.actionClicked(col.ddOptions.iconAction, col.ddOptions.iconActionParams) }></button>
                                        }
                                    </div>
                                  : col.label ?
                                    <div key={'col-' + index} className={colClassName}>
                                        <p className="col--value">
                                          { 
                                            col.link ?
                                                (
                                                    col.targetBlank ? 
                                                        <a href={col.link} target="_blank">{col.value}</a>
                                                    :
                                                        <Link to={col.link}>{isNaN(col.value) ? col.value.toString().replace(/_|\./g, " ") : col.value.toString().replace(/_/g, " ")}</Link> 
                                                ) 
                                            : 
                                            col.dateFormat ? <Moment format={col.dateFormat}>{col.value}</Moment> 
                                            : 
                                            col.showAsCheckbox ? <label className="chk-container"><input type="checkbox" disabled checked={col.value} /><span className="checkmark"></span></label>
                                            :
                                            (col.value || col.value === 0) ? (isNaN(col.value) ? col.value.toString().replace(/_|\./g, " ") : col.value.toString().replace(/_/g, " "))
                                            :
                                            ''
                                          }
                                        </p>
                                        <p className="col--label">{col.label}</p>
                                    </div>
                                :
                                    ''
                                )
                            })
                        }
                    </div>
                </div>
                

            </div>
        );
    }
}

export default withCookies(TableRow);
