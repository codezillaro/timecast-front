import React, { Component } from 'react';
import { Modal, ModalBody, ModalFooter, ModalHeader, FormGroup } from 'reactstrap';
import { withCookies } from 'react-cookie';
import { Formik, Form, ErrorMessage } from 'formik';

class AddClient extends Component {

    render() {
        return (
            <div>
                <Modal isOpen={this.props.showPopup} centered wrapClassName="component--modal-new-project">
                    <Formik
                        initialValues = {{ clientId: this.props.clientId, clientName: this.props.clientName, hogarthVisible: this.props.hogarthVisible }}
                        validate={values => {
                            let errors = {};
                            if (!values.clientName) {
                                errors.clientName = 'Required';
                            }
                            return errors;
                        }}
                        onSubmit={(values, { setSubmitting }) => {
                            this.props.saveClient(values);
                            setSubmitting(false);
                        }}
                        >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            /* and other goodies */
                        }) => (
                            <Form>
                                <ModalHeader>Add new client</ModalHeader>
                                <ModalBody className="new--project">
                                    <input type="hidden" name="clientId" value={values.clientId}/>
                                    <FormGroup>
                                        <label htmlFor="client--name" className="text-left--ts">Name</label>
                                        <input
                                            type="text"
                                            name="clientName"
                                            id="client--name"
                                            value={values.clientName}
                                            onChange={handleChange}
                                            className={errors.clientName ? 'form-control error' : 'form-control'}
                                            placeholder="Name"
                                        />
                                        <ErrorMessage name="clientName" className="form-error" component="div" />
                                    </FormGroup>

                                    <FormGroup>
                                        <label className="chk-container">Hogarth visible
                                            <input type="checkbox" name="hogarthVisible" checked={values.hogarthVisible} onChange={handleChange} id="client--hogarth-visible" />
                                            <span className="checkmark"></span>
                                        </label>
                                    </FormGroup>    
                                
                                </ModalBody>
                                <ModalFooter>
                                    <button type="submit" disabled={isSubmitting} className="btn--ts-primary btn--ts">{isSubmitting ? 'SAVING ...' : 'SAVE CLIENT'}</button>
                                    <button type="button" className="btn--ts-grey btn--ts" onClick={this.props.closeModal}>CANCEL</button>
                                </ModalFooter>
                            </Form>
                        )}
                    </Formik>
                </Modal>
            </div>
        );
    }
}

export default withCookies(AddClient);