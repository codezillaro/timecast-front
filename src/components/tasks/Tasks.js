import React, { Component } from 'react';
import './Tasks.scss';
import { withCookies } from 'react-cookie';
class TasksNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskList:
            [
                {
                    "title": "New Task",
                    "taskIsDone": "0/0",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "0",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Sketch",
                    "cssActionClass": "orange TaskTitle",
                    "id": "1",
                },

                {
                    "title": "Budget and contract",
                    "taskIsDone": "0/3",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "0",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Sketch",
                    "cssActionClass": "orange TaskTitle",
                    "id": "2",
                },

                {
                    "title": "Search for a UI kit",
                    "taskIsDone": "2/9",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "7",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Spotify",
                    "cssActionClass": "green TaskTitle",
                    "id": "3",
                },

                {
                    "title": "Design new dashboard",
                    "taskIsDone": "3/5",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "2",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Spotify",
                    "cssActionClass": "green TaskTitle",
                    "id": "4",
                },

                {
                    "title": "Design search page",
                    "taskIsDone": "4/6",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "8",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Spotify",
                    "cssActionClass": "green TaskTitle",
                    "id": "5",
                },

                {
                    "title": "Review created screens",
                    "taskIsDone": "3/3",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "4",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Dribble",
                    "cssActionClass": "pink TaskTitle",
                    "id": "6",
                },

                {
                    "title": "Prepare HTML &amp; CSS",
                    "taskIsDone": "0/2",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "1",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Dribble",
                    "cssActionClass": "pink TaskTitle",
                    "id": "7",
                },

                {
                    "title": "Browser testing",
                    "taskIsDone": "2/2",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "0",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Dribble",
                    "cssActionClass": "pink TaskTitle",
                    "id": "7",
                },

                {
                    "title": "Fix issues",
                    "taskIsDone": "5/9",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "2",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Dribble",
                    "cssActionClass": "pink TaskTitle",
                    "id": "8",
                },

                {
                    "title": "Budget and contract",
                    "taskIsDone": "4/4",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "3",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Dribble",
                    "cssActionClass": "pink TaskTitle",
                    "id": "9",
                },

                {
                    "title": "Search for a UI kit",
                    "taskIsDone": "0/1",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "1",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Behance",
                    "cssActionClass": "blue TaskTitle",
                    "id": "10",
                },

                {
                    "title": "Design search page",
                    "taskIsDone": "0/2",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "2",
                    "cssCommentsIcon": "la la-comments",
                    "action": "Behance",
                    "cssActionClass": "blue TaskTitle",
                    "id": "11",
                },

                {
                    "title": "Review created screens",
                    "taskIsDone": "1/2",
                    "cssTaskIsDoneIcon": "la la-list-alt",
                    "comments": "2",
                    "cssCommentsIcon": "la la-comments",
                    "action": "IOTASK",
                    "cssActionClass": "lightGrey TaskTitle",
                    "id": "11",
                },
                
            ]
        }
    }
    render() {
        return (

        <div className="TasksNewContainer">
            {
                this.state.taskList.map(function (item) {
                    return (
                        <div className="TaskNewTable row" key={'task-' + item.id}>
                            <div className="col-md-8">
                                <div className="TaskContainer row">
                                    <div className="TaskNameContainer col-md-12">
                                    <div className="TaskNameContainer">
                                        <label className="chk-container">
                                            {item.title}
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="row">
                                    <div className="col-md-4  task-items-container">
                                        <p><i className={item.cssTaskIsDoneIcon}></i> {item.taskIsDone} </p>
                                    </div>
                                    <div className="col-md-4 task-items-container">
                                        <p><i className={item.cssCommentsIcon}></i> {item.comments} </p>
                                    </div>
                                    <div className="col-md-4 task-action">
                                        <p className={item.cssActionClass}>{item.action} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
        );
    }
}

export default withCookies(TasksNew);
