import React, { Component } from 'react';
import { Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Label, Col, Row } from 'reactstrap';
import './AddProject.scss';
import { withCookies } from 'react-cookie';
import { Formik, Form, ErrorMessage } from 'formik';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import Preloader from "../../components/preloader/Preloader";
import { toast } from 'react-toastify';
import { createAction } from '../../utils/utils';

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class AddProject extends Component {

    constructor(props) {
        super(props);
        let today = new Date();

        this.state = {
            fileName: 'UPLOAD FILE',
            file: new FormData(),
            redirectToLogin: false,
            loading: false,
            clientsList: [],
            brandsList: [],
            clientPMsList: [],
            typesList: [],
            idgPMsList: [],
            ownersList: [],
            projectEditId: -1,
            projectEditJN: '',
            projectEditTitle: '',
            projectEditDescription: '',
            projectEditFile: '',
            projectEditClientId: -1,
            projectEditHogarthPMId: -1,
            projectEditBrandId: -1,
            projectEditOwnerId: -1,
            projectEditTypeId: -1,
            projectEditStartDate: today,
            projectEditEndDate: new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000), //one week after today
            projectEditFixedPrice: '',
            renderForm: false
        };
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleDeadline = this.handleDeadline.bind(this);
    }

    componentDidMount = () => {
        this.getClientsList();
        this.getBrandsList();
        this.getClientPMsList();
        this.getProjectTypesList();
        this.getIdgPMsList();
        this.getOwners();

        if(this.props.projectId && this.props.projectId != -1) {
            //get details, it's an update
            this.getProjectDetails(this.props.projectId);
        }
        else {
            this.setState({
                renderForm: true
            })
        }
    }

    /* PROJECT DETAILS */
    getProjectDetails = (projectId) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project/' + projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                projectEditId: res.data.data.id,
                projectEditJN: res.data.data.job_number,
                projectEditTitle: res.data.data.title,
                projectEditDescription: res.data.data.description || '',
                projectEditFile: res.data.data.filepath || '',
                projectEditClientId: res.data.data.client_id,
                projectEditHogarthPMId: res.data.data.hogarth_pm_id || -1,
                projectEditBrandId: res.data.data.brand_id,
                projectEditOwnerId: res.data.data.owner_id,
                projectEditTypeId: res.data.data.type_id,
                projectEditStartDate: new Date(res.data.data.start_date),
                projectEditEndDate: new Date(res.data.data.deadline),
                projectEditFixedPrice: res.data.data.fixed_price || '',
                renderForm: true,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
     
    handleStartDate = (date) => {
        this.setState({
            projectEditStartDate: date
        });
    }

    handleDeadline = (date) => {
        this.setState({
            projectEditEndDate: date
        });
    }

    handleFileChange = e => {
        this.setState({
            fileName: e.target.files[0].name,
            file: e.target.files[0]
        })
    }

    /* Save Project */
    saveProject = (projectData, startDate, endDate, file) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        let formData = new FormData();
        if(file){
            formData.append("file_path", file);
        }
        formData.append('job_number', projectData.projectJN);
        formData.append('title', projectData.projectTitle);
        
        if(projectData.projectDescription && projectData.projectDescription.length){
            formData.append('description', projectData.projectDescription);
        }

        formData.append('brand_id', projectData.projectBrandId);
        formData.append('client_id', projectData.projectClientId);
        formData.append('start_date', startDate.getFullYear() + '-' + ("0" + parseInt(parseInt(startDate.getMonth()) + 1)).slice(-2) + '-' + ("0" + startDate.getDate()).slice(-2));
        formData.append('deadline', endDate.getFullYear() + '-' + ("0" + parseInt(parseInt(endDate.getMonth()) + 1)).slice(-2) + '-' + ("0" + endDate.getDate()).slice(-2));
        formData.append('owner_id', projectData.projectOwnerId);
        formData.append('type_id', projectData.projectTypeId);

        if(projectData.projectHogarthPMId && projectData.projectHogarthPMId != -1){
            formData.append('hogarth_pm_id', projectData.projectHogarthPMId);
        }
        if(projectData.projectFixedPrice && projectData.projectFixedPrice.length){
            formData.append('fixed_price', projectData.projectJN);
        }
        
        axios({
            method: "POST",
            url: process.env.REACT_APP_API_URL + 'projects/' + (projectData.projectId != -1 ? projectData.projectId : ''),
            data: formData,
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken'),
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => {
            this.setState({
                loading: false,
            })
            this.props.closeEditModal(true);
            toast.success("Project has been saved.");

            createAction(2, 'projects', res.data.data.id, cookies.get('authToken'));
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
                this.props.closeEditModal(true);
            }
            toast.error("An error occured, please try again later.");
        })
    }

    /* PROJECT LISTS */
    getClientsList = () => {
        /* Clients - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'clients', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                clientsList: res.data.data.paginatedClients,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getBrandsList = () => {
        /* Brands - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'brands', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                brandsList: res.data.data.paginatedBrands,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getClientPMsList = () => {
        /* Client PMs - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'users-by-role/3', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                clientPMsList: res.data.data.users,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getProjectTypesList = () => {
        /* Project Types - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project-types', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                typesList: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getIdgPMsList = () => {
        /* IDG PMs - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'users-by-role/2', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                idgPMsList: res.data.data.users,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getOwners = () => {
        const { cookies } = this.props;
        this.setState({
            loading: true
        })
        axios.get(process.env.REACT_APP_API_URL + 'owners', {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                ownersList: res.data.data.users,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    render() {
        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div>
                { this.state.loading && <Preloader/> }
                <Modal isOpen={this.props.showPopup} wrapClassName="component--modal-new-project">
                {this.state.renderForm &&
                    <Formik
                            initialValues = {{ 
                                projectId: this.state.projectEditId,
                                projectJN: this.state.projectEditJN, 
                                projectTitle: this.state.projectEditTitle,
                                projectDescription: this.state.projectEditDescription || '', 
                                projectClientId: this.state.projectEditClientId || -1,
                                projectHogarthPMId: this.state.projectEditHogarthPMId || -1,
                                projectBrandId: this.state.projectEditBrandId || -1,
                                projectTypeId: this.state.projectEditTypeId || -1, 
                                projectOwnerId: this.state.projectEditOwnerId || -1,
                                projectFile: '',
                                projectFixedPrice: this.state.projectEditFixedPrice
                            }}
                            validate={values => {
                                // console.log(values);
                                let errors = {};
                                if (!values.projectJN) {
                                    errors.projectJN = 'Required';
                                }
                                if (!values.projectTitle) {
                                    errors.projectTitle = 'Required';
                                }
                                if (!values.projectClientId || values.projectClientId === -1) {
                                    errors.projectClientId = 'Required';
                                }
                                if (!values.projectBrandId || values.projectBrandId === -1) {
                                    errors.projectBrandId = 'Required';
                                }
                                if (!this.state.projectEditStartDate) {
                                    errors.projectStartDate = 'Required';
                                }
                                if (!this.state.projectEditEndDate) {
                                    errors.projectEndDate = 'Required';
                                }
                                if (!values.projectTypeId || values.projectTypeId === -1) {
                                    errors.projectTypeId = 'Required';
                                }
                                if (!values.projectOwnerId || values.projectOwnerId === -1) {
                                    errors.projectOwnerId = 'Required';
                                }
                                return errors;
                            }}
                            onSubmit={(values, { setSubmitting }) => {
                                this.saveProject(values, new Date(this.state.projectEditStartDate), new Date(this.state.projectEditEndDate), this.state.file);
                                setSubmitting(false);
                            }}
                            >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                /* and other goodies */
                            }) => (
                                <Form>
                                    <input type="hidden" name="projectId" value={values.projectId}/>
                                    <ModalHeader toggle={this.toggle}>Add new project</ModalHeader>
                                    <ModalBody className="new--project">
                                        <FormGroup>
                                            <label htmlFor="project--jn" className="text-left--ts">Job Number</label>
                                            <input
                                                type="text"
                                                name="projectJN"
                                                id="project--jn"
                                                value={values.projectJN}
                                                onChange={handleChange}
                                                className={errors.projectJN ? 'form-control error' : 'form-control'}
                                                placeholder="Job number"
                                            />
                                            <ErrorMessage name="projectJN" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--title" className="text-left--ts">Title</label>
                                            <input
                                                type="text"
                                                name="projectTitle"
                                                id="project--title"
                                                value={values.projectTitle}
                                                onChange={handleChange}
                                                className={errors.projectTitle ? 'form-control error' : 'form-control'}
                                                placeholder="Title"
                                            />
                                            <ErrorMessage name="projectTitle" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--description" className="text-left--ts">Description</label>
                                            <textarea
                                                name="projectDescription"
                                                id="project--description"
                                                value={values.projectDescription}
                                                onChange={handleChange}
                                                className={errors.projectDescription ? 'form-control error' : 'form-control'}
                                                placeholder="Description"
                                            />
                                            <ErrorMessage name="projectDescription" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--file" className="text-left--ts">File</label>
                                            <div className="file-input-placeholder">
                                                <i className="la la-cloud-upload"></i> {this.state.fileName}
                                                <input
                                                    type="file"
                                                    name="projectFile"
                                                    id="project--file"
                                                    value={values.projectFile}
                                                    onChange={this.handleFileChange}
                                                    className={errors.projectFile ? 'form-control-file error' : 'form-control-file'}
                                                />
                                            </div>
                                            <ErrorMessage name="projectFile" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--client" className="text-left--ts">Client</label>
                                            <select
                                                name="projectClientId"
                                                id="project--client"
                                                value={values.projectClientId}
                                                onChange={handleChange}
                                                className={errors.projectClientId ? 'form-control error' : 'form-control'}
                                            >
                                                <option>Select</option>
                                                {
                                                    this.state.clientsList.map(client => {
                                                        return (
                                                            <option key={'client-' + client.id} value={client.id}>{client.name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <ErrorMessage name="projectClientId" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--hogarth-pm" className="text-left--ts">Hogarth PM</label>
                                            <select
                                                name="projectHogarthPMId"
                                                id="project--hogarth-pm"
                                                value={values.projectHogarthPMId}
                                                onChange={handleChange}
                                                className={errors.projectHogarthPMId ? 'form-control error' : 'form-control'}
                                            >
                                                <option>Select</option>
                                                {
                                                    this.state.clientPMsList.map(hgPM => {
                                                        return (
                                                            !hgPM.disabled?
                                                            <option key={'hg-pm-' + hgPM.id} value={hgPM.id}>{hgPM.name}</option>
                                                            :
                                                            ''
                                                        )
                                                    })
                                                }
                                            </select>
                                            <ErrorMessage name="projectHogarthPMId" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--brand" className="text-left--ts">Brand</label>
                                            <select
                                                name="projectBrandId"
                                                id="project--brand"
                                                value={values.projectBrandId}
                                                onChange={handleChange}
                                                className={errors.projectBrandId ? 'form-control error' : 'form-control'}
                                            >
                                                <option>Select</option>
                                                {
                                                    this.state.brandsList.map(brand => {
                                                        return (
                                                            <option key={'brand-' + brand.id} value={brand.id}>{brand.name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <ErrorMessage name="projectBrandId" className="form-error" component="div" />
                                        </FormGroup>

                                        <FormGroup>
                                            <Row className="start-deadline-row" form>
                                                <Col md={6}>
                                                    <Label className="text-left--ts"><span>Start date </span></Label>
                                                    <DatePicker
                                                        selected={this.state.projectEditStartDate}
                                                        onChange={this.handleStartDate}
                                                        className="start-date-col"
                                                    />
                                                    <ErrorMessage name="projectStartDate" className="form-error" component="div" />
                                                </Col>
                                                <Col md={6}>
                                                    <Label className="text-left--ts"><span>Deadline </span></Label>
                                                    <DatePicker
                                                        selected={this.state.projectEditEndDate}
                                                        onChange={this.handleDeadline}
                                                        className="start-date-col"
                                                    />
                                                    <ErrorMessage name="projectEndDate" className="form-error" component="div" />
                                                </Col>
                                            </Row>
                                        </FormGroup>

                                        <FormGroup>
                                            <label htmlFor="project--type" className="text-left--ts">Type </label>
                                            <select
                                                name="projectTypeId"
                                                id="project--type"
                                                value={values.projectTypeId}
                                                onChange={handleChange}
                                                className={errors.projectTypeId ? 'form-control error' : 'form-control'}
                                            >
                                                <option>Select</option>
                                                {
                                                    this.state.typesList.map(type => {
                                                        return (
                                                            <option key={'type-' + type.id} value={type.id}>{type.name}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            <ErrorMessage name="projectTypeId" className="form-error" component="div" />
                                        </FormGroup>

                                        {values.projectTypeId == 1 &&
                                            <FormGroup>
                                                <label htmlFor="project--fixed-price" className="text-left--ts">Fixed Price </label>
                                                <input
                                                    type="text"
                                                    name="projectFixedPrice"
                                                    id="project--fixed-price"
                                                    value={values.projectFixedPrice}
                                                    onChange={handleChange}
                                                    className={errors.projectFixedPrice ? 'form-control error' : 'form-control'}
                                                    placeholder="Fixed price"
                                                />
                                                <ErrorMessage name="projectFixedPrice" className="form-error" component="div" />
                                            </FormGroup>
                                        }
                                        
                                        <FormGroup>
                                            <label htmlFor="project--owner" className="text-left--ts">Owner </label>
                                            <select
                                                name="projectOwnerId"
                                                id="project--owner"
                                                value={values.projectOwnerId}
                                                onChange={handleChange}
                                                className={errors.projectOwnerId ? 'form-control error' : 'form-control'}
                                            >
                                                <option>Select</option>
                                                {
                                                    this.state.ownersList.map(owner => {
                                                        return (
                                                            !owner.disabled ?
                                                            <option key={'owner-' + owner.id} value={owner.id}>{owner.name}</option>
                                                            :''
                                                        )
                                                    })
                                                }
                                            </select>
                                            <ErrorMessage name="projectOwnerId" className="form-error" component="div" />
                                        </FormGroup>
                                    </ModalBody>
                                    <ModalFooter>
                                        <button type="submit" disabled={isSubmitting} className="btn--ts-primary btn--ts">{isSubmitting ? 'SAVING ...' : 'SAVE PROJECT'}</button>
                                        <button type="button" className="btn--ts-grey btn--ts" onClick={this.props.closeEditModal}>CANCEL</button>
                                    </ModalFooter>
                                </Form>
                            )}
                        </Formik>
                    }
                </Modal>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(AddProject));