import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import axios from 'axios';
import TableRow from "../../components/tableRow/TableRow";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import Preloader from "../../components/preloader/Preloader";
import AddPO from "../../components/addPO/AddPO";
import confirm from 'reactstrap-confirm';
import { toast } from 'react-toastify';

class PurchaseOrders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            purchaseOrders: [],
            costEstimates: [],
            editPOId: -1,
            showAddPO: false
        };
    }

    componentDidMount() {
        this.getPurchaseOrders();
        this.getCostEstimates();
    }

    openModal = () => {
        this.setState({
            showAddPO: true
        });
    }

    closeModal = refreshList => {
        this.setState({
            showAddPO: false,
            editPOId: -1
        });
        if(refreshList) {
            this.getPurchaseOrders();
        }
    }

    editCE = (poId) => {
        this.setState({
            editPOId: poId,
            showAddPO: true
        });
    }

    getCostEstimates = () => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'cost-estimates/' + this.props.projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                costEstimates: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    getPurchaseOrders = () => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'purchase-orders/' + this.props.projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                purchaseOrders: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    downloadInvoice = () => {

    }
    stornoInvoice = () => {
        
    }

    deletePO = async (poId) => {
        const { cookies } = this.props;

        let result = await confirm({
            title: 'Are you sure?',
            message: "This action is irreversible",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            this.setState({
                loading: true
            })

            axios.delete(process.env.REACT_APP_API_URL + 'purchase-orders/' + poId, {
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getPurchaseOrders();
                this.newPO();
                toast.success("Purchase order has been removed.");
            })
            .catch(err => {
                if(err.response.status === 401){
                    //redirect to login
                    this.props.toggleRedirectToLogin(true);
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
        }
    }

    newPO = () => {
        this.props.newPO()
    }

    render() {
        return (
            <div className="project-subsection">
                { this.state.loading && <Preloader/> }
                <div className="row project-subsection--head">
                    <div className="col-10"><h1>Purchase orders</h1></div>
                    <div className="col-2 ta-right">
                        <UncontrolledDropdown>
                            <DropdownToggle nav>
                                <i className="la la-plus-circle"></i>
                            </DropdownToggle>
                            {this.state.costEstimates.length &&
                                <DropdownMenu right>
                                    <DropdownItem key={'po-1'} onClick={(e) => {this.openModal()}}>New purchase order</DropdownItem>
                                </DropdownMenu>
                            }
                        </UncontrolledDropdown>
                    </div>
                </div>

                {this.state.showAddPO &&
                    <AddPO
                        poId={this.state.editPOId}
                        projectId={this.props.projectId}
                        closeEditModal={this.closeModal}
                        showPopup={this.state.showAddPO}
                    />
                }

                <div className="project-subsection--body">
                    {this.state.purchaseOrders.map((po, index) => {
                        return(
                            <TableRow
                                key={'porow-' + index}
                                tableCols={
                                    [
                                        {
                                            value: po.id,
                                            label: 'ID'
                                        },
                                        {
                                            value: po.cost_estimate_id,
                                            label: 'Cost Estimate'
                                        },
                                        {
                                            value: po.po_number,
                                            label: 'PO number'
                                        },
                                        {
                                            value: po.date,
                                            label: 'Date',
                                            dateFormat: 'DD.MM.YYYY'
                                        },
                                        {
                                            value: po.client_pm_name,
                                            label: 'Client PM',
                                            customCssClass: 'row-details'
                                        },
                                        {
                                            value: po.idg_pm_name,
                                            label: 'PM',
                                            customCssClass: 'row-details'
                                        },
                                        {
                                            value: po.value,
                                            label: 'Value'
                                        },
                                        {
                                            value: po.client_unit_name,
                                            label: 'Client unit',
                                            customCssClass: 'row-details'
                                        },
                                        {
                                            value: po.po_status_name,
                                            label: 'Status',
                                            customCssClass: (po.status_id == 2 ? 'danger' : 'success')
                                        },
                                        {
                                            value: po.invoice_id,
                                            label: 'Invoice ID'
                                        },
                                        {
                                            ddOptions: [
                                                {
                                                    value: 'Remove purchase order',
                                                    action: {
                                                        name: 'deletePO',
                                                        params: po.id
                                                    }
                                                },
                                                {
                                                    value: 'Download invoice',
                                                    action: {
                                                        name: 'downloadInvoice',
                                                        params: po.id
                                                    }
                                                },
                                                {
                                                    value: 'Storno invoice',
                                                    action: {
                                                        name: 'stornoInvoice',
                                                        params: po.id
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                                deletePO={this.deletePO}
                                downloadInvoice={this.downloadInvoice}
                                stornoInvoice={this.stornoInvoice}
                            />
                        )
                    })}
                    
                    {!this.state.costEstimates.length && 
                        <div className="no-data">
                            <p><i className="la la-exclamation-triangle"></i> No cost estimates</p>
                        </div>
                    }

                    {this.state.costEstimates.length && !this.state.purchaseOrders.length ? 
                        <div className="no-data">
                            <p><i className="la la-exclamation-triangle"></i> No purchase orders</p>
                            <button type="button" className="btn--ts-primary btn--ts" onClick={(e) => {this.openModal()}}>Add your first purchase order for this project</button>
                        </div>
                    : ''}
                    
                </div>
            </div>
        );
    }
}

export default withCookies(PurchaseOrders);