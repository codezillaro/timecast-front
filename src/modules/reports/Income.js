import React, { Component } from 'react';
import './Reports.scss';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import TableRow from "../../components/tableRow/TableRow";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import { Area, AreaChart, ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import NumberFormat from 'react-number-format';

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class Income extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            reportData: [],
            reportDetails: []
        }
    }

    componentDidMount() {
        const { cookies } = this.props;

        if(cookies.get('userRole') != 2) {
            this.props.toggleRedirectToLogin(true);
        }

        this.getReportData();
    }

    getReportData = () => {
        const {cookies} = this.props;

        this.setState({
            loading: true
        });

        axios.get(process.env.REACT_APP_API_URL + 'reports-income', {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            let repData = [];
            for(let i in res.data.data) {
                repData.push({
                    name: i,
                    income: res.data.data[i]
                })
            }
            
            this.setState({
                loading: false,
                reportData: repData
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    getDetailedReport = (index) => {
        if(index && index.activeLabel) {
            let dataParams = index.activeLabel.split("-");
            // year: dataParams[0]
            // month: dataParams[1] 

            const {cookies} = this.props;

            this.setState({
                loading: true
            });

            axios.get(process.env.REACT_APP_API_URL + 'reports-income-by-month', {
                params: {
                    year: dataParams[0],
                    month: dataParams[1]
                },
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                console.log(res);
                this.setState({
                    loading: false,
                    reportDetails: res.data.data
                });
            })
            .catch(err => {
                if(err.response.status === 401){
                    //redirect to login
                    this.props.toggleRedirectToLogin(true);
                } else {
                    this.setState({
                        loading: false
                    })
                }
            })
        }
    }

    render() {
        const NumberMask = (value) => (
            <div>
                <NumberFormat
                    value={parseInt(value)}
                    displayType="text"
                    thousandSeparator="."
                    decimalSeparator=","
                    prefix="€"
                />
                <p className="footnote-click">click for more details</p>
            </div>
          )

        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar clearfix">
                { this.state.loading && <Preloader/> }
                <Sidebar selectedItem="7" selectedSubmenu="71"/>
                <div className="page--header-and-content">
                    <Header
                        pageTitle="Reports::Income" 
                    />
                    <div className="ReportsPage">
                        <div className="responsive-chart">
                            <ResponsiveContainer>
                                <AreaChart data={this.state.reportData} onClick={this.getDetailedReport}>
                                    <defs>
                                        <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="0%" stopColor="#4d7cfe" stopOpacity={0.9}/>
                                        <stop offset="100%" stopColor="#4d7cfe" stopOpacity={0}/>
                                        </linearGradient>
                                    </defs>
                                    <XAxis dataKey="name" />
                                    <YAxis type="number" tickLine={false} tickFormatter={(tick) => ((tick / 1000) + 'k')} />
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <Tooltip formatter={NumberMask} />
                                    <Area type="monotone" dataKey="income" stroke="#4d7cfe" fillOpacity={1} fill="url(#colorUv)" />
                                </AreaChart>
                            </ResponsiveContainer> 
                        </div>

                        {this.state.reportDetails &&
                            <div className="detailed-chart">
                                {this.state.reportDetails.map((po, index) => {
                                    return (
                                        <TableRow
                                            key={'po-details-' + index}
                                            tableCols={
                                                [
                                                    {
                                                        value: po.project_title,
                                                        label: 'Project',
                                                        link: '/project/' + po.id,
                                                        mobileFullWidth: true,
                                                        customCssClass: 'row-details row-head-mobile'
                                                    },
                                                    {
                                                        value: po.date,
                                                        dateFormat: 'DD.MM.YYYY',
                                                        label: 'Date'
                                                    },
                                                    {
                                                        value: po.po_number,
                                                        label: 'PO number',
                                                        hiddenMobile: true
                                                    },
                                                    {
                                                        value: po.client_unit_name,
                                                        label: 'Client unit',
                                                        customCssClass: 'row-details',
                                                        hiddenMobile: true
                                                    },
                                                    {
                                                        value: po.client_name,
                                                        label: 'Client',
                                                        customCssClass: 'row-details'
                                                    },
                                                    {
                                                        value: po.client_pm_name,
                                                        label: 'Client\'s PM',
                                                        customCssClass: 'row-details',
                                                        hiddenMobile: true
                                                    },
                                                    {
                                                        value: '€'+po.value,
                                                        label: 'Value'
                                                    },
                                                    {
                                                        value: '#'+po.invoice_id,
                                                        label: 'Invoice',
                                                        link: 'http://netlunch.infodesign.ro/facturi/index.php?page=factura_pdf&id=' + po.invoice_id + '&tip=0&nr_chitanta=&plaja_chitanta=&hash=' + po.hash_factura,
                                                        targetBlank: true
                                                    },
                                                    {
                                                        value: '#'+po.invoice_date,
                                                        label: 'Invoice date',
                                                        dateFormat: 'DD.MM.YYYY',
                                                        hiddenMobile: true
                                                    }
                                                ]
                                            }
                                        /> 
                                    )
                                })}
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Income));
