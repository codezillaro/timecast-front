import React, { Component } from 'react';
import './Reports.scss';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import TableRow from "../../components/tableRow/TableRow";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import { ResponsiveContainer, Tooltip, PieChart, Pie, Cell } from 'recharts';

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class MissingPOs extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            reportData: [],
            reportDetails: [],
            reportByPM: [],
            reportByClientPM: [],
            reportByProjectType: [],
            totalMissingValue: 0
        }
    }

    componentDidMount() {
        const { cookies } = this.props;

        if(cookies.get('userRole') != 2) {
            this.props.toggleRedirectToLogin(true);
        }

        this.getReportData();
    }

    getReportData = () => {
        const {cookies} = this.props;

        this.setState({
            loading: true
        });

        axios.get(process.env.REACT_APP_API_URL + 'reports-missing-pos', {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            console.log(res);
            let repByPM = [];
            let repByClientPM = [];
            let repByProjectType = [];
            let totalMissing = 0;

            for(let i in res.data.data) {
                repByPM[res.data.data[i].idg_pm_name] = (repByPM[res.data.data[i].idg_pm_name] || 0) + parseInt(res.data.data[i].value);
                repByClientPM[res.data.data[i].client_pm_name] = (repByClientPM[res.data.data[i].client_pm_name] || 0) + parseInt(res.data.data[i].value);
                repByProjectType[res.data.data[i].project_type_name] = (repByProjectType[res.data.data[i].project_type_name] || 0) + parseInt(res.data.data[i].value);
                totalMissing += parseInt(res.data.data[i].value);
            }

            let reportByPM = [];
            for(let i in repByPM) {
                if(repByPM[i] != 0) {
                    reportByPM.push({
                        name: i,
                        value: repByPM[i]
                    })
                }
            }

            let reportByClientPM = [];
            for(let i in repByClientPM) {
                if(repByClientPM[i] != 0) {
                    reportByClientPM.push({
                        name: i,
                        value: repByClientPM[i]
                    })
                }
            }

            let reportByProjectType = [];
            for(let i in repByProjectType) {
                if(repByProjectType[i] != 0) {
                    reportByProjectType.push({
                        name: i,
                        value: repByProjectType[i]
                    })
                }
            }
            
            this.setState({
                loading: false,
                reportData: res.data.data,
                reportByPM: reportByPM,
                reportByClientPM: reportByClientPM,
                reportByProjectType: reportByProjectType,
                totalMissingValue: totalMissing
            });
        })
        .catch(err => {
            if(err && err.response && err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    render() {

        const COLORS = ['#0043fe', '#4173ff', '#608afd', '#81a3fe', '#a4bcff', '#c4d4ff', '#002da9', '#002489', '#3955a2', '#061a52', '#5971b6', '#6886d9'];

        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar clearfix">
                { this.state.loading && <Preloader/> }
                <Sidebar selectedItem="7" selectedSubmenu="72"/>
                <div className="page--header-and-content">
                    <Header
                        pageTitle="Reports::Missing POs" 
                    />
                    <div className="ReportsPage">
                        <div className="total-missing">
                            <span className="label">Total missing value: </span>{this.state.totalMissingValue.toLocaleString()} EUR
                        </div>

                        <div className="responsive-chart">
                            <div className="row">
                                <div className="col-md-4">
                                    <span className="chart--custom-label">Project managers</span>
                                    <div className="chart">
                                        <ResponsiveContainer>
                                            <PieChart>
                                                <Pie
                                                    data={this.state.reportByPM}
                                                    fill="#4D7CFE"
                                                    dataKey="value"
                                                    >
                                                    {
                                                        this.state.reportByPM.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                                    }
                                                </Pie>
                                                <Tooltip/>
                                            </PieChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <span className="chart--custom-label">Client PMs</span>
                                    <div className="chart">
                                        <ResponsiveContainer>
                                        <PieChart>
                                                <Pie
                                                    data={this.state.reportByClientPM}
                                                    fill="#4D7CFE"
                                                    dataKey="value"
                                                    >
                                                    {
                                                        this.state.reportByClientPM.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                                    }
                                                </Pie>
                                                <Tooltip/>
                                            </PieChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <span className="chart--custom-label">Project types</span>
                                    <div className="chart">
                                        <ResponsiveContainer>
                                            <PieChart>
                                                <Pie
                                                    data={this.state.reportByProjectType}
                                                    fill="#4D7CFE"
                                                    dataKey="value"
                                                    >
                                                    {
                                                        this.state.reportByProjectType.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                                    }
                                                </Pie>
                                                <Tooltip/>
                                            </PieChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        {this.state.reportData &&
                            <div className="detailed-chart">
                                {this.state.reportData.map((po, index) => {
                                    // console.log(po);
                                    return (
                                        <TableRow
                                            key={'po-details-' + index}
                                            tableCols={
                                                [
                                                    {
                                                        value: po.project_job_number || '',
                                                        label: 'JN',
                                                        link: '/project/' + po.project_id,
                                                        customCssClass: 'row-details row-head-mobile'
                                                    },
                                                    {
                                                        value: po.project_title || '',
                                                        label: 'Project',
                                                        link: '/project/' + po.project_id,
                                                        customCssClass: 'row-details row-head-mobile'
                                                    },
                                                    {
                                                        value: po.project_status_name,
                                                        label: 'Project status'
                                                    },
                                                    {
                                                        value: po.project_type_name,
                                                        label: 'Project type'
                                                    },
                                                    {
                                                        value: po.brand_name,
                                                        label: 'Brand'
                                                    },
                                                    {
                                                        value: po.client_pm_name,
                                                        label: 'Client\'s PM',
                                                        customCssClass: 'row-details'
                                                    },
                                                    {
                                                        value: po.idg_pm_name,
                                                        label: 'PM',
                                                        customCssClass: 'row-details'
                                                    },
                                                    {
                                                        value: '€'+po.value,
                                                        label: 'Value'
                                                    },
                                                    {
                                                        value: '#'+po.created_at,
                                                        label: 'Created',
                                                        dateFormat: 'DD.MM.YYYY'
                                                    }
                                                ]
                                            }
                                        /> 
                                    )
                                })}
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(MissingPOs));
