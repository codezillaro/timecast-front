import React, { Component } from 'react';
import './Login.scss';
import {Link} from "react-router-dom";
import { Formik } from 'formik';
import * as Yup from 'yup';
import Preloader from "../../components/preloader/Preloader";
import Particles from 'react-particles-js';
import {Animated} from "react-animated-css";
import { withCookies } from 'react-cookie';
import { withRouter } from "react-router-dom";
import { compose } from 'recompose';
import axios from 'axios';
import { connect } from "react-redux";
import { toggleRedirectToLogin } from "../../actions/index";
import { Redirect } from 'react-router-dom'

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            loading: false,
            errorMessage: '',
            redirectToHomepage: false
        };
    }

    setRedirect = () => {
        this.setState({
            redirectToHomepage: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirectToHomepage) {
            return <Redirect to='/' />
        }
    }

    handleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleSubmit = event => {
        event.preventDefault();

        this.setState({
            loading: true
        });

        let loginData = JSON.stringify({
            email: this.state.email,
            password: this.state.password
        })

        axios.post(process.env.REACT_APP_API_URL + 'login', loginData, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.data.data.token){
                const { cookies } = this.props;
                cookies.set('authToken', res.data.data.token, { path: '/' });
                cookies.set('userRole', res.data.data.role, { path: '/' });
                this.setState({
                    errorMessage: '',
                    loading: false
                })
                this.setRedirect();
                
            }
            else{
                this.setState({
                    errorMessage: 'A aparut o eroare',
                    loading: false
                });
            }
        })
        .catch(err => {
            this.setState({
                errorMessage: err.message,
                loading: false
            });
        });
            
    };

    componentDidMount(){
        document.body.className = "login";
    }

    render() {
        const LoginSchema = Yup.object().shape({
            email: Yup.string()
                .email('Invalid email address')
                .required('Required'),
            password: Yup.string()
                .min(6, 'Must be longer than 6 characters')
                .required('Required')
        });

        return (
            this.state.loading ? <Preloader/> :
            <div className="LoginPage">
                {this.renderRedirect()}
                <div className="particles--component">
                    <Particles params={{
                        particles: {
                            number: {
                              value: 30,
                              density: {
                                enable: true,
                                value_area: 800
                              }
                            },
                            color: {
                              value: "#cccccc"
                            },
                            shape: {
                              type: "circle",
                              stroke: {
                                width: 0,
                                color: "#FFFFFF"
                              },
                              polygon: {
                                nb_sides: 5
                              }
                            },
                            opacity: {
                              value: 1,
                              random: false,
                              anim: {
                                enable: false,
                                speed: 1,
                                opacity_min: 0.1,
                                sync: false
                              }
                            },
                            size: {
                              value: 5,
                              random: true,
                              anim: {
                                enable: false,
                                speed: 80,
                                size_min: 0.1,
                                sync: false
                              }
                            },
                            line_linked: {
                              enable: true,
                              distance: 300,
                              color: "#cccccc",
                              opacity: 0.3,
                              width: 1
                            },
                            move: {
                              enable: true,
                              speed: 6,
                              direction: "none",
                              random: false,
                              straight: false,
                              out_mode: "out",
                              bounce: false,
                              attract: {
                                enable: false,
                                rotateX: 600,
                                rotateY: 1200
                              }
                            }
                        },
                        interactivity: {
                            detect_on: "canvas",
                            events: {
                                onhover: {
                                    enable: false,
                                    mode: "repulse"
                                },
                                onclick: {
                                    enable: true,
                                    mode: "push"
                                },
                                resize: true
                            },
                            modes: {
                                grab: {
                                    distance: 800,
                                    line_linked: {
                                        opacity: 1
                                    }
                                },
                                bubble: {
                                    distance: 800,
                                    size: 80,
                                    duration: 2,
                                    opacity: 0.4,
                                    speed: 3
                                },
                                repulse: {
                                    distance: 400,
                                    duration: 0.4
                                },
                                push: {
                                    particles_nb: 4
                                },
                                remove: {
                                    particles_nb: 2
                                }
                            }
                        },
                        "retina_detect": true
                    }}/>
                </div>
                <Animated animationIn="fadeInLeft" animationOut="fadeOutLeft" isVisible={true}>
                    <div className={"login-wrapper"}>
                        <div className="full-center centerer">
                            <Formik
                                validationSchema = {LoginSchema}
                            >
                                { props => {
                                    const {
                                        touched,
                                        errors,
                                        isSubmitting
                                    } = props;
                                    return (
                                        <form onSubmit={this.handleSubmit}>
                                            <div className="form-group">
                                                <label htmlFor="email">
                                                    Email
                                                </label>
                                                <input
                                                    id="email"
                                                    name="email"
                                                    placeholder="Enter your email"
                                                    value={this.state.email}
                                                    type="text"
                                                    onChange={this.handleChange}
                                                    className={
                                                        errors.email && touched.email ? 'form-control error' : 'form-control'
                                                    }
                                                />

                                                {errors.email &&
                                                touched.email && <div className="input-feedback">{errors.email}</div>}
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="password">
                                                    Password
                                                </label>
                                                <input
                                                    id="password"
                                                    name="password"
                                                    placeholder="Password"
                                                    value={this.state.password}
                                                    type="password"
                                                    onChange={this.handleChange}
                                                    className={
                                                        errors.password && touched.password ? 'form-control error' : 'form-control'
                                                    }
                                                />

                                                {errors.password &&
                                                touched.password && <div className="input-feedback">{errors.password}</div>}
                                            </div>
                                            
                                            <div className="form-error">{this.state.errorMessage}</div>

                                            <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                                                SIGN IN
                                            </button>

                                            <div className="remember-forgot">
                                                <Link to="/forgot" className="link-register">Forgot your password?</Link>
                                            </div>
                                        </form>
                                    );
                                } }
                            </Formik>
                        </div>
                    </div>
                </Animated>
            </div>
        );
    }
}

export default connect(null, mapDispatchToProps)(compose(
    withRouter,
    withCookies
  )(Login));