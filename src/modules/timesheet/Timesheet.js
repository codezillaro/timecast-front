import React, { Component } from 'react';
import './Timesheet.scss';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import TableRow from "../../components/tableRow/TableRow";
import AddTimesheet from "../../components/addTimesheet/AddTimesheet";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import confirm from 'reactstrap-confirm';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import Moment from 'react-moment';
import { toast } from 'react-toastify';

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class Timesheet extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            tsEntries: [],
            tsWeek: 'x',
            startDate: '',
            endDate: '',
            userRole: -1,
            editTEId: -1
        }
    }

    componentDidMount() {
        const { cookies } = this.props;

        //role = 2 for superadmin or idg pm and 1 for devs
        this.setState({
            userRole: cookies.get('userRole')
        })

        this.getTSEntries();
    }

    getTSEntries = () => {
        const {cookies} = this.props;

        axios.get(process.env.REACT_APP_API_URL + 'ts-by-week/' + this.state.tsWeek, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                tsEntries: res.data.data,
                loading: false,
                startDate: res.data.startDate,
                endDate: res.data.endDate,
                tsWeek: res.data.weekNo,
                showAddTimesheet: false,
                editTEId: -1
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    pageChanged = (increment) => {
        this.setState({
            loading: true
        })

        let weekNo = this.state.tsWeek;
        this.setState(
            { tsWeek: parseInt(weekNo) + increment },
            () => {
                this.getTSEntries();
            }
        );
    }

    openModal = () => {
        this.setState({
            showAddTimesheet: true
        });
    }

    closeModal = refreshList => {
        this.setState({
            showAddTimesheet: false,
            editTEId: -1
        });
        if(refreshList) {
            this.getTSEntries();
        }
    }

    editTSEntry = (teId) => {
        this.setState({
            editTEId: teId,
            showAddTimesheet: true
        })
    }

    deleteTSEntry = async (teId) => {
        const { cookies } = this.props;
        let result = await confirm({
            title: 'Are you sure?',
            message: "This action is irreversible",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            this.setState({
                loading: true
            })

            axios.delete(process.env.REACT_APP_API_URL + 'ts/' + teId, {
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getTSEntries();
                toast.success("Timesheet entry has been deleted.");
            })
            .catch(err => {
                if(err.response.status === 401){
                    //redirect to login
                    this.props.toggleRedirectToLogin(true);
                } else {
                    this.setState({
                        loading: false
                    })
                }
                toast.error("An error occured, please try again later.");
            })
        }
    }

    render() {
        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar clearfix">
                { this.state.loading && <Preloader/> }
                <Sidebar/>
                <div className="page--header-and-content">
                    <Header
                        pageTitle="Timesheet"
                        headerOptions={[{
                            name: 'New Entry',
                            action: 'openModal', 
                            params: true
                        }]}
                        roleNeeded={1}
                        openModal={this.openModal}
                    />
                    <div className="TSPage">

                        <div className="ts-head">
                            <div className="ts-pagination">
                                <button className="btn-pag btn-pag--prev" onClick={() => this.pageChanged(-1)}><i className="la la-chevron-circle-left"></i></button>
                                <span>Week: {this.state.startDate} - {this.state.endDate}</span>
                                <button className="btn-pag btn-pag--next" onClick={() => this.pageChanged(1)}><i className="la la-chevron-circle-right"></i></button>
                            </div>
                        </div>

                        {this.state.userRole == 1 &&
                            <div className="ts-form">
                                <button onClick={(e) => { this.openModal() }} className="btn-new-container">
                                    <i className="la la-plus-circle"></i>
                                    New entry
                                    {this.state.showAddTimesheet &&
                                        <AddTimesheet
                                            teId={this.state.editTEId != -1 ? this.state.editTEId : ''}
                                            closeModal={this.closeModal}
                                            showPopup={this.state.showAddTimesheet}
                                        />
                                    }
                                </button>
                            </div>
                        }

                        {
                            Object.keys(this.state.tsEntries).map((dailyEntries, key) => {
                                return (
                                    <div key={'day-' + key} className="TableMainContainer">
                                        <div className={this.state.tsEntries[dailyEntries].total_hours < 8 ? 'daily-summary danger' : 'daily-summary success'}><Moment format={'dddd DD.MM.YYYY'}>{dailyEntries}</Moment> - <span className="col--value">{this.state.tsEntries[dailyEntries].total_hours} hours</span></div>
                                        {
                                            this.state.tsEntries[dailyEntries].timesheet_entries.map((item, index) => {
                                                return (
                                                    <TableRow
                                                        key = {'row-' + index}
                                                        tableCols = {
                                                            [
                                                                {
                                                                    value: item.project_title,
                                                                    label: 'Project',
                                                                    link: '/project/' + item.project_id,
                                                                    customCssClass: 'row-details'
                                                                },
                                                                {
                                                                    value: item.manual_date,
                                                                    dateFormat: 'DD.MM.YYYY', //momentjs formats
                                                                    label: 'Date'
                                                                },
                                                                {
                                                                    value: item.user,
                                                                    label: 'User'
                                                                },
                                                                {
                                                                    value: item.hours,
                                                                    label: 'Hours'
                                                                },
                                                                {
                                                                    value: item.activity_name,
                                                                    label: 'Activity',
                                                                    customCssClass: 'row-details'
                                                                },
                                                                {
                                                                    value: item.extra_request,
                                                                    label: 'Extra',
                                                                    showAsCheckbox: true
                                                                },
                                                                {
                                                                    value: item.qa_issue,
                                                                    label: 'QA',
                                                                    showAsCheckbox: true
                                                                },
                                                                {
                                                                    value: item.overtime,
                                                                    label: 'OT',
                                                                    showAsCheckbox: true
                                                                },
                                                                {
                                                                    value: item.details,
                                                                    label: 'Details',
                                                                    customCssClass: 'row-details'
                                                                },
                                                                this.state.userRole == 1 ? {
                                                                    ddOptions: [
                                                                        {
                                                                            value: 'Edit entry',
                                                                            action: {
                                                                                name: 'editTSEntry',
                                                                                params: item.id
                                                                            }
                                                                        },
                                                                        {
                                                                            value: 'Remove entry',
                                                                            action: {
                                                                                name: 'deleteTSEntry',
                                                                                params: item.id
                                                                            }
                                                                        }
                                                                    ]
                                                                } : {}
                                                            ]
                                                        }
                                                        editTSEntry={this.editTSEntry}
                                                        deleteTSEntry={this.deleteTSEntry}
                                                    />
                                                )
                                            })
                                        }
                                    </div>
                                )
                            }
                        )}
                    </div>    
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Timesheet));
