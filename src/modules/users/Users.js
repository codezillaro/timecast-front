import React, { Component } from 'react';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import Paginate from "../../components/paginate/Paginate";
import AddUser from "../../components/addUser/AddUser";
import TableRow from "../../components/tableRow/TableRow";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class Users extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            usersList: [],
            locations: [],
            activityTypes: [],
            loading: false,
            page: 1,
            paginationKey: 0, //used to reset pagination when status changed
            totalResults: 0,
            resultsPerPage: 10,
            searchString: '',
            showAddUser: false,
            editUserId: '',
            editUserName: '',
            editUserEmail: '',
            editUserPhone: '',
            editUserLocationId: 1,
            editUserActivityTypes: []
        }

        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        this.getUsers();
        this.getLocations();
        this.getActivityTypes();
    }

    openModal = (event, clearData) => {
        if (clearData) {
            this.setState({
                editUserId: '',
                editUserName: '',
                editUserEmail: '',
                editUserPhone: '',
                editUserLocationId: 1,
                editUserActivityTypes: []
            })
        }
        this.setState({
            showAddUser: true
        });
    }

    closeModal = event => {
        this.setState({
            showAddUser: false
        });
    }

    pageChanged = pageNo => {
        this.setState(
            { page: pageNo },
            () => {
                this.getUsers();
            }
        );
    }

    searchUsersByName = event => {
        let searchStr = event.target.value;

        this.setState({
            searchString: searchStr
        }, () => {
            this.getUsers();
        });

    }

    getUsers = () => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'users-by-role/' + this.props.roleId, {
            params: {
                page: this.state.page,
                resPerPage: this.state.resultsPerPage,
                searchString: this.state.searchString
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                usersList: res.data.data.users,
                totalResults: res.data.data.totalResults,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    editUser = (userId) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'user/' + userId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                loading: false,
                editUserId: res.data.data.id,
                editUserName: res.data.data.name,
                editUserEmail: res.data.data.email,
                editUserPhone: res.data.data.phone,
                editUserLocationId: res.data.data.location_id,
                editUserActivityTypes: res.data.data.UserActivities,
                showAddUser: true
            })
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    saveUser = (userData) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        //if we have a user id, it means we should update. so, the method for our request it's PUT. else, it will be a simple POST
        let requestMethod = "POST";
        if (userData.userId) {
            requestMethod = "PUT";
        }

        axios({
            method: requestMethod,
            url: process.env.REACT_APP_API_URL + 'users/' + userData.userId || '',
            data: {
                name: userData.userName,
                email: userData.userEmail,
                phone: userData.userPhone,
                location_id: userData.userLocationId
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            let userId = res.data.data.id;

            //save user's activities
            axios({
                method: "POST",
                url: process.env.REACT_APP_API_URL + 'user-activity-types',
                data: {
                    user_id: userId,
                    user_activities: this.state.editUserActivityTypes
                },
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                console.log(res);
            })

            if(requestMethod == 'POST'){
                //this user is a new one => save user's role
                axios({
                    method: "POST",
                    url: process.env.REACT_APP_API_URL + 'role',
                    data: {
                        user_id: userId,
                        role_id: userData.roleId
                    },
                    headers: {
                        'Authorization': 'Bearer ' + cookies.get('authToken')
                    }
                })
                .then(res => {
                    this.getUsers();
                    this.setState({
                        loading: false,
                        showAddUser: false
                    })
                })
                .catch(err => {
                    if(err.response.status === 401){
                        //redirect to login
                        this.props.toggleRedirectToLogin(true);
                    } else {
                        this.setState({
                            loading: false,
                            showAddUser: false
                        })
                    }
                })
            }
            else{
                //just load the users list again
                this.getUsers();
                this.setState({
                    loading: false,
                    showAddUser: false
                })
            }
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false,
                    showAddUser: false
                })
            }
        })
    }

    deleteUser = async (userId) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.delete(process.env.REACT_APP_API_URL + 'users/' + userId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.getUsers();
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    getLocations = () => {
        const { cookies } = this.props;
        axios.get(process.env.REACT_APP_API_URL + 'locations', {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                locations: res.data.data
            })
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            }
        })
    }
    getActivityTypes = () => {
        const { cookies } = this.props;
        axios.get(process.env.REACT_APP_API_URL + 'activity-types', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                activityTypes: res.data.data.paginatedActivityTypes
            })
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            }
        })
    }

    removeAT = (atId) => {
        let found = -1;
        let auxEditUserATs = this.state.editUserActivityTypes;
        for(let i = 0; i < auxEditUserATs.length; i++){
            if(auxEditUserATs[i].id == atId){
                found = i;
            }
        }
        if(found != -1) {
            auxEditUserATs.splice(found, 1);
            this.setState({
                editUserActivityTypes: auxEditUserATs
            });
        }
    }

    addAT = (atId) => {
        let found = -1;
        let auxEditUserATs = this.state.editUserActivityTypes;
        for(let i = 0; i < auxEditUserATs.length; i++){
            if(auxEditUserATs[i].id == atId){
                found = i;
            }
        }
        if(found == -1) {
            let atIndex = -1;
            for(let i = 0; i < this.state.activityTypes.length; i++){
                if(this.state.activityTypes[i].id == atId){
                    atIndex = i;
                }
            }
            auxEditUserATs.push(this.state.activityTypes[atIndex]);
            this.setState({
                editUserActivityTypes: auxEditUserATs
            });
        }
    }

    render() {
        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar clearfix">
                {this.state.loading && <Preloader />}
                <Sidebar />
                <div className="page--header-and-content">
                    <Header />
                    <div className="UsersPage">

                        <div className="TableHead">
                            <div className="TableHead--options">
                                <div className="row">
                                    <div className="th-searchbar">
                                        <i className="la la-search"></i>
                                        <input type="text" onChange={this.searchUsersByName} placeholder='Search users by name' />
                                    </div>
                                </div>
                            </div>

                            <a href="#" onClick={(e) => { this.openModal(e, true) }} className="btn-new-container">
                                <i className="la la-plus-circle"></i>
                                Add {this.props.roleId == 2 ? 'Infodesign PM' : this.props.roleId == 3 ? 'Hogarth PM' : 'Developer'}
                                <AddUser
                                    userId={this.state.editUserId}
                                    userName={this.state.editUserName}
                                    userEmail={this.state.editUserEmail}
                                    userPhone={this.state.editUserPhone}
                                    roleId={this.props.roleId}
                                    locations={this.state.locations}
                                    activityTypes={this.state.activityTypes}
                                    userLocationId={this.state.editUserLocationId}
                                    editUserActivityTypes={this.state.editUserActivityTypes}
                                    removeAT={this.removeAT}
                                    addAT={this.addAT}
                                    saveUser={this.saveUser}
                                    closeModal={this.closeModal}
                                    showPopup={this.state.showAddUser}
                                />
                            </a>

                        </div>
                        
                        <div className="TableMainContainer">
                            {
                                this.state.usersList.map((item, index) => {
                                    return (
                                        (this.props.roleId == 2 || this.props.roleId == 3) ?
                                            <TableRow
                                                key={'row-' + index}
                                                tableCols={
                                                    [
                                                        {
                                                            value: item.name,
                                                            label: 'Name',
                                                            mobileFullWidth: true,
                                                            customCssClass: 'row-head-mobile'
                                                        },
                                                        {
                                                            value: item.email,
                                                            label: 'Email'
                                                        },
                                                        {
                                                            value: item.phone,
                                                            label: 'Phone'
                                                        },
                                                        {
                                                            value: !item.disabled,
                                                            label: 'Status',
                                                            showAsCheckbox: true
                                                        },
                                                        {
                                                            ddOptions: [
                                                                {
                                                                    value: 'Edit this user',
                                                                    action: {
                                                                        name: 'editUser',
                                                                        params: item.id
                                                                    }
                                                                },
                                                                {
                                                                    value: item.disabled ? 'Activate this user' : 'Inactivate this user',
                                                                    action: {
                                                                        name: 'deleteUser',
                                                                        params: item.id
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                                editUser={this.editUser}
                                                deleteUser={this.deleteUser}
                                            />
                                        :
                                        <TableRow
                                            key={'row-' + index}
                                            tableCols={
                                                [
                                                    {
                                                        value: item.name,
                                                        label: 'Name',
                                                        mobileFullWidth: true,
                                                        customCssClass: 'row-head-mobile'
                                                    },
                                                    {
                                                        value: item.UserActivities.map(activity => { return (' ' + activity.name) }),
                                                        label: 'Activity types'
                                                    },
                                                    {
                                                        value: item.location,
                                                        label: 'Location'
                                                    },
                                                    {
                                                        value: item.email,
                                                        label: 'Email'
                                                    },
                                                    {
                                                        value: item.phone,
                                                        label: 'Phone'
                                                    },
                                                    {
                                                        value: !item.disabled,
                                                        label: 'Status',
                                                        showAsCheckbox: true
                                                    },
                                                    {
                                                        ddOptions: [
                                                            {
                                                                value: 'Edit this user',
                                                                action: {
                                                                    name: 'editUser',
                                                                    params: item.id
                                                                }
                                                            },
                                                            {
                                                                value: item.disabled ? 'Activate this user' : 'Inactivate this user',
                                                                action: {
                                                                    name: 'deleteUser',
                                                                    params: item.id
                                                                }
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                            editUser={this.editUser}
                                            deleteUser={this.deleteUser}
                                        />
                                    )
                                })
                            }
                        </div>
                        <Paginate key={this.state.paginationKey} triggerPageChange={this.pageChanged} pagesCounter={Math.ceil(this.state.totalResults / this.state.resultsPerPage)} />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Users));