import React, { Component } from 'react';
import './ProjectDetails.scss';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import {withRouter} from 'react-router';
import { compose } from 'recompose';
import Moment from 'react-moment';
import AddProject from "../../components/addProject/AddProject";
import AddParent from "../../components/addParent/AddParent";
import CostEstimates from "../../components/costEstimates/CostEstimates";
import { toast } from 'react-toastify';
import confirm from 'reactstrap-confirm';
import ProjectResources from '../../components/projectResources/ProjectResources';
import TimesheetEntries from '../../components/timesheetEntries/TimesheetEntries';
import PurchaseOrders from '../../components/purchaseOrders/PurchaseOrders';
import { createAction } from '../../utils/utils';

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class ProjectDetails extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        const { match : {params}} = this.props;

        this.state = {
            projectId: params.projectId,
            projectData: {},
            showAddResource: false,
            showAddParent: false,
            userRole: -1,
            posNumber: 0
        }
    }

    componentDidMount(){
        const { cookies } = this.props;
        // console.log(this.props.match);
        const { match : {params}} = this.props;

        //role = 2 for superadmin or idg pm and 1 for devs
        this.setState({
            userRole: cookies.get('userRole')
        })

        this.getProjectDetails(params.projectId);
        createAction(1, 'projects', params.projectId, cookies.get('authToken'));
    }

    /* PROJECT DETAILS */
    getProjectDetails = (projectId) => {
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project/' + projectId, {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            console.log(res);
            this.setState({
                projectData: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    /* CLOSE PROJECT */
    toggleProject = (projectId, statusId) => {
        const { cookies } = this.props;

        let formData = new FormData();
        formData.append('status_id', statusId);

        axios({
            method: "POST",
            url: process.env.REACT_APP_API_URL + 'projects/' + projectId,
            data: formData,
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken'),
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => {
            this.setState({
                loading: false,
            })
            toast.success("Project's status been changed.");
            this.getProjectDetails(projectId);
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    openEditModal = () => {
        this.setState({
            showAddProject: true
        });
    }

    closeEditModal = refreshList => {
        this.setState({
            showAddProject: false
        });
        if(refreshList) {
            const { match : {params}} = this.props;
            this.getProjectDetails(params.projectId);
        }
    }

    openParentModal = () => {
        this.setState({
            showAddParent: true
        });
    }
    closeParentModal = refreshProject => {
        this.setState({
            showAddParent: false
        });
        if(refreshProject) {
            const { match : {params}} = this.props;
            this.getProjectDetails(params.projectId);
        }
    }

    removeParent = async () => {
        const { cookies } = this.props;

        let result = await confirm({
            title: 'Are you sure?',
            message: "You are about to remove the parent of this project",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            axios({
                method: "DELETE",
                url: process.env.REACT_APP_API_URL + 'project-parents/' + this.state.projectData.id,
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getProjectDetails(this.state.projectData.id);
                toast.success("Project's parent has been removed.");
            })
        }
    }

    removeChild = async (subprojectId) => {
        const { cookies } = this.props;

        let result = await confirm({
            title: 'Are you sure?',
            message: "You are about to remove a child of this project",
            confirmText: "DELETE",
            confirmColor: "primary",
            cancelColor: "text-danger"
        });

        if (result) {
            axios({
                method: "DELETE",
                url: process.env.REACT_APP_API_URL + 'project-child/' + this.state.projectData.id + '/' + subprojectId,
                headers: {
                    'Authorization': 'Bearer ' + cookies.get('authToken')
                }
            })
            .then(res => {
                this.getProjectDetails(this.state.projectData.id);
                toast.success("Subproject has been removed.");
            })
        }
    }

    newPO = () => {
        let auxPosNumber = this.state.posNumber;
        this.setState({
            posNumber: auxPosNumber + 1
        })
        console.log("Here");
    }

    render() {
        let startDate = new Date(this.state.projectData.start_date);
        let deadline = new Date(this.state.projectData.deadline);
        let today = new Date();

        let totalDaysDiff = Math.abs(deadline.getTime() - startDate.getTime());
        let remainingDaysDiff = Math.abs(today.getTime() - startDate.getTime());
        let totalDays = Math.ceil(totalDaysDiff / (1000 * 3600 * 24)); 
        let remainingDays = Math.ceil(remainingDaysDiff / (1000 * 3600 * 24)); 
        let remPercent = remainingDays / totalDays;
        let widthPercent = remPercent > 1 ? '100%' : remPercent * 100 + '%';
        let statusColor = remPercent < 0.5 ? 'green' : remPercent < 0.75 ? 'orange': 'red';

        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar">
                { this.state.loading && <Preloader/> }
                <Sidebar/>
                <div className="page--header-and-content">
                    {this.state.projectData.title &&
                    <Header
                        pageTitle={this.state.projectData.title}
                        headerOptions={[
                            {
                                name: 'Edit project',
                                action: 'openEditModal', 
                                params: true,
                                iconClass: 'la la-pencil'
                            }
                        ]}
                        roleNeeded={2}
                        openEditModal={this.openEditModal}
                    />
                    }
                    <div className="ProjectDetailsPage">
                        <div className="row project-details-row">
                            <div className={this.state.userRole == 2 ? 'col-md-6' : 'col'}>
                                <div className="project-subsection">
                                    <div className="row project-subsection--head">
                                        <div className="col-10"><h1>Project summary</h1></div>
                                        <div className="col-2 ta-right">
                                            {this.state.userRole == 2 &&
                                                <UncontrolledDropdown>
                                                    <DropdownToggle nav>
                                                        <i className="la la-plus-circle"></i>
                                                    </DropdownToggle>
                                                    <DropdownMenu right>
                                                        <DropdownItem key={'ps-1'} onClick={this.openEditModal}>Edit this project</DropdownItem>
                                                        
                                                        {!this.state.projectData.parent_id && !this.state.projectData.has_children ?
                                                        <DropdownItem key={'ps-2'} onClick={this.openParentModal}>Set parent for this project</DropdownItem>
                                                        : this.state.projectData.parent_id ?
                                                        <DropdownItem key={'ps-2'} onClick={this.removeParent}>Remove parent</DropdownItem>
                                                        : ''
                                                        }
                                                        {this.state.projectData.status_id != 3 ?
                                                        <DropdownItem key={'ps-3'} onClick={() => this.toggleProject(this.state.projectData.id, 3)}>Close this project</DropdownItem>
                                                        :
                                                        <DropdownItem key={'ps-3'} onClick={() => this.toggleProject(this.state.projectData.id, 2)}>Open this project</DropdownItem>
                                                        }
                                                    </DropdownMenu>
                                                </UncontrolledDropdown>
                                            }
                                        </div>
                                        <i className={'project-completion ' + statusColor} style={{width: widthPercent}}></i>

                                        {this.state.showAddProject &&
                                            <AddProject
                                                projectId={this.state.projectData.id}
                                                closeEditModal={this.closeEditModal}
                                                showPopup={this.state.showAddProject}
                                            />
                                        }
                                        {this.state.showAddParent &&
                                            <AddParent
                                                projectId={this.state.projectData.id}
                                                closeModal={this.closeParentModal}
                                                showPopup={this.state.showAddParent}
                                            />
                                        }
                                    </div>

                                    <div className="project-subsection--body">
                                        <div className="proj-details">
                                            <p className="proj-details--label">JN</p>
                                            <p className="proj-details--value">{this.state.projectData.job_number}</p>
                                        </div>
                                        <div className="proj-details">
                                            <p className="proj-details--label">Title</p>
                                            <p className="proj-details--value">{this.state.projectData.title}</p>
                                        </div>

                                        {this.state.projectData.description &&
                                        <div className="proj-details">
                                            <p className="proj-details--label">Description</p>
                                            <p className="proj-details--value">{this.state.projectData.description}</p>
                                        </div>
                                        }

                                        <div className="proj-details">
                                            <div className="row">
                                                <div className="col-6">
                                                    <p className="proj-details--label">Brand</p>
                                                    <p className="proj-details--value">{this.state.projectData.brand_name}</p>
                                                </div>
                                                <div className="col-6">
                                                    <p className="proj-details--label">Client</p>
                                                    <p className="proj-details--value">{this.state.projectData.client_name}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="proj-details">
                                            <div className="row">
                                                {this.state.projectData.hg_pm_name &&
                                                <div className="col-6">
                                                    <p className="proj-details--label">Client PM</p>
                                                    <p className="proj-details--value">{this.state.projectData.hg_pm_name}</p>
                                                </div>
                                                }
                                                <div className="col-6">
                                                    <p className="proj-details--label">IDG PM</p>
                                                    <p className="proj-details--value">{this.state.projectData.idg_pm_name}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="proj-details">
                                            <p className="proj-details--label">Owner</p>
                                            <p className="proj-details--value">{this.state.projectData.owner_name}</p>
                                        </div>

                                        <div className="proj-details">
                                            <div className="row">
                                                <div className="col-6">
                                                    <p className="proj-details--label">Start date</p>
                                                    <p className="proj-details--value"><Moment format={'DD.MM.YYYY'}>{new Date(this.state.projectData.start_date)}</Moment></p>
                                                </div>
                                                <div className="col-6">
                                                    <p className="proj-details--label">Deadline</p>
                                                    <p className="proj-details--value"><Moment format={'DD.MM.YYYY'}>{new Date(this.state.projectData.deadline)}</Moment></p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div className="proj-details">
                                            <p className="proj-details--label">Type</p>
                                            <p className="proj-details--value">{this.state.projectData.type_name}</p>
                                        </div>

                                        {this.state.projectData.settle_title &&
                                        <div className="proj-details">
                                            <p className="proj-details--label">Settle</p>
                                            <p className="proj-details--value"><a href={'/project/' + this.state.projectData.parent_id}>{this.state.projectData.settle_title}</a></p>
                                        </div>
                                        }

                                        {this.state.projectData.children &&
                                        <div className="proj-details">
                                            <p className="proj-details--label">Subprojects</p>
                                            <ul className="subprojects--list">
                                                {this.state.projectData.children.map((subproject, index) => {
                                                    return(
                                                        <li key={'sub-' + index}>
                                                            {this.state.userRole == 2 &&
                                                                <button onClick={() => this.removeChild(subproject.id)}><i className="la la-trash-o"></i></button> 
                                                            }
                                                            <a href={'/project/' + subproject.id}>{subproject.title}</a>
                                                        </li>
                                                    )
                                                })}
                                            </ul>
                                        </div>
                                        }
                                    </div>    
                                </div>
                            </div>
                            
                            {this.state.userRole == 2 &&
                                <div className="col-md-6">
                                    {this.state.projectData.id &&
                                        <ProjectResources
                                            projectId={this.state.projectData.id}
                                        />
                                    }
                                </div>
                            }
                        </div>
                        
                        {this.state.userRole == 2 &&
                            <div className="project-details-row">
                                {this.state.projectData.id && !this.state.projectData.parent_id &&
                                    <CostEstimates
                                        projectId={this.state.projectData.id}
                                        posNumber={this.state.posNumber}
                                    />
                                }
                            </div>
                        }
                        {this.state.userRole == 2 &&
                            <div className="project-details-row">
                                {this.state.projectData.id && !this.state.projectData.parent_id &&
                                    <PurchaseOrders 
                                        projectId={this.state.projectData.id}
                                        newPO={this.newPO}
                                    />
                                }
                            </div>
                        }
                        {!this.state.projectData.has_children &&
                        <div className="project-details-row">
                            {this.state.projectData.id &&
                                <TimesheetEntries 
                                    projectId={this.state.projectData.id}
                                />
                            }
                        </div>
                        }

                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(compose(
    withRouter,
    withCookies
  )(ProjectDetails));
