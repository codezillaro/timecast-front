import React, { Component } from 'react';
import './Projects.scss';
import Header from "../../components/header/Header";
import Sidebar from "../../components/sidebar/Sidebar";
import Paginate from "../../components/paginate/Paginate";
import TableRow from "../../components/tableRow/TableRow";
import Preloader from "../../components/preloader/Preloader";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";
import axios from 'axios';
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { toggleRedirectToLogin } from "../../actions/index";
import AddProject from "../../components/addProject/AddProject";

const mapStateToProps = state => {
    return { redirectToLogin: state.redirectToLogin };
}

const mapDispatchToProps = dispatch => {
    return {
        toggleRedirectToLogin: redirectValue => dispatch(toggleRedirectToLogin(redirectValue))
    };
}

class Projects extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            projectList: [],
            projectStatuses: [],
            clientsList: [], //pm filters
            brandsList: [], //pm filters
            clientPMsList: [], //pm filters
            projectTypesList: [], //pm filters
            idgPMsList: [], //pm filters
            loading: false,
            page: 1,
            filtersStatus: -1,
            filtersClientId: -1,
            filtersBrandId: -1,
            filtersClientPMId: -1,
            filtersProjectTypeId: -1,
            filtersIdgPMId: -1,
            filtersTitle: '',
            filtersJN: '',
            paginationKey: 0, //used to reset pagination when status changed
            totalResults: 0,
            resultsPerPage: 50,
            showAddProject: false,
            userRole: -1,
            redirectToLogin: false,
            editProjectId: -1
        }
    }

    componentDidMount(){
        this.getProjectsList();
        this.getProjectStatuses();

        const { cookies } = this.props;

        //role = 2 for superadmin or idg pm and 1 for devs
        this.setState({
            userRole: cookies.get('userRole')
        })

        if(cookies.get('userRole') == 2){
            this.getClientsList();
            this.getBrandsList();
            this.getClientPMsList();
            this.getProjectTypesList();
            this.getIdgPMsList();
        }
    }

    openModal = () => {
        this.setState({
            showAddProject: true
        });
    }

    closeModal = refreshList => {
        this.setState({
            showAddProject: false,
            editProjectId: -1
        });
        if(refreshList) {
            this.getProjectsList();
        }
    }

    filtersChanged = (filterId, filterValue) => {
        this.setState(
            { 
                [filterId]: filterValue,
                page: 1, // reset pagination when status filter is changed
                paginationKey: new Date().getTime() //unique key to reset pagination component
            },
            () => {
                this.getProjectsList();
            });
    }

    pageChanged = pageNo => {
        this.setState(
            { page: pageNo },
            () => {
                this.getProjectsList();
            }
        );
    }

    getProjectsList = () => {
        /* Projects List */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'projects', {
            params: {
                page: this.state.page,
                resPerPage: this.state.resultsPerPage,
                status: this.state.filtersStatus != -1 ? this.state.projectStatuses[this.state.filtersStatus].id : -1,
                clientId: this.state.filtersClientId != -1 ? this.state.clientsList[this.state.filtersClientId].id : -1,
                brandId: this.state.filtersBrandId != -1 ? this.state.brandsList[this.state.filtersBrandId].id : -1,
                clientPM: this.state.filtersClientPMId != -1 ? this.state.clientPMsList[this.state.filtersClientPMId].id : -1,
                type: this.state.filtersProjectTypeId != -1 ? this.state.projectTypesList[this.state.filtersProjectTypeId].id : -1,
                pm: this.state.filtersIdgPMId != -1 ? this.state.idgPMsList[this.state.filtersIdgPMId].id : -1,
                title: this.state.filtersTitle,
                jn: this.state.filtersJN
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                projectList: res.data.data.projects,
                totalResults: res.data.data.totalResults,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    /* Filter Options & Lists */
    getProjectStatuses = () => {
        /* Project Statuses - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project-statuses', {
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                projectStatuses: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    editProject = (projectId) => {
        this.setState({
            showAddProject: true,
            editProjectId: projectId
        });
    }

    /* PROJECT LISTS */
    getClientsList = () => {
        /* Clients - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'clients', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                clientsList: res.data.data.paginatedClients,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getBrandsList = () => {
        /* Brands - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'brands', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                brandsList: res.data.data.paginatedBrands,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getClientPMsList = () => {
        /* Client PMs - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'users-by-role/3', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                clientPMsList: res.data.data.users,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getProjectTypesList = () => {
        /* Project Types - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'project-types', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                projectTypesList: res.data.data,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }
    getIdgPMsList = () => {
        /* IDG PMs - Dropdown Options */
        const { cookies } = this.props;

        this.setState({
            loading: true
        })

        axios.get(process.env.REACT_APP_API_URL + 'users-by-role/2', {
            params: {
                page: 0,
                resPerPage: 1000
            },
            headers: {
                'Authorization': 'Bearer ' + cookies.get('authToken')
            }
        })
        .then(res => {
            this.setState({
                idgPMsList: res.data.data.users,
                loading: false
            });
        })
        .catch(err => {
            if(err.response.status === 401){
                //redirect to login
                this.props.toggleRedirectToLogin(true);
            } else {
                this.setState({
                    loading: false
                });
            }
        })
    }

    render() {
        return (
            this.props.redirectToLogin ? <Redirect to="/login"/> :
            <div className="page--with-header-sidebar">
                { this.state.loading && <Preloader/> }
                <Sidebar/>
                <div className="page--header-and-content">
                    <Header
                        pageTitle="Projects"
                        headerOptions={[{
                            name: 'New Project',
                            action: 'openModal', 
                            params: true
                        }]}
                        roleNeeded={2}
                        openModal={this.openModal}
                    />
                    <div className="ProjectsPage">

                        <div className="TableHead">
                            <div className="TableHead--options">

                                <div className="filter">
                                    <div className="filter--label">
                                        <i className="la la-line-chart"></i>
                                        Status:
                                    </div>
                                    <div className="filter--dd">
                                        <UncontrolledDropdown>
                                        <DropdownToggle nav>
                                            <div>
                                            { this.state.filtersStatus != -1 ? this.state.projectStatuses[this.state.filtersStatus].name : 'Select' }
                                            <i className="la la-angle-down"></i>
                                            </div>
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            {
                                            this.state.projectStatuses.map((status, index) => {
                                                return (
                                                <div key={'status-' + index} onClick={() => this.filtersChanged("filtersStatus", index)}>
                                                    <DropdownItem>{status.name}</DropdownItem>
                                                </div>
                                                )
                                            })
                                            }
                                        </DropdownMenu>
                                        </UncontrolledDropdown>
                                    </div>
                                </div>
                                
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-filter"></i>
                                            Job number:
                                        </div>
                                        <div className="filter--dd">
                                            <input type="text" onChange={(event) => this.filtersChanged("filtersJN", event.target.value)}></input>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-keyboard-o"></i>
                                            Title:
                                        </div>
                                        <div className="filter--dd">
                                            <input type="text" onChange={(event) => this.filtersChanged("filtersTitle", event.target.value)}></input>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-money"></i>
                                            Client:
                                        </div>
                                        <div className="filter--dd">
                                            <UncontrolledDropdown>
                                            <DropdownToggle nav>
                                                <div>
                                                { this.state.filtersClientId != -1 ? this.state.clientsList[this.state.filtersClientId].name.substr(0, 10) + '...' : 'Select' }
                                                <i className="la la-angle-down"></i>
                                                </div>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                this.state.clientsList.map((client, index) => {
                                                    return (
                                                    <div key={'client-' + index} onClick={() => this.filtersChanged("filtersClientId", index)}>
                                                        <DropdownItem>{client.name}</DropdownItem>
                                                    </div>
                                                    )
                                                })
                                                }
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-registered"></i>
                                            Brand:
                                        </div>
                                        <div className="filter--dd">
                                            <UncontrolledDropdown>
                                            <DropdownToggle nav>
                                                <div>
                                                { this.state.filtersBrandId != -1 ? this.state.brandsList[this.state.filtersBrandId].name : 'Select' }
                                                <i className="la la-angle-down"></i>
                                                </div>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                this.state.brandsList.map((brand, index) => {
                                                    return (
                                                    <div key={'brand-' + index} onClick={() => this.filtersChanged("filtersBrandId", index)}>
                                                        <DropdownItem>{brand.name}</DropdownItem>
                                                    </div>
                                                    )
                                                })
                                                }
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-male"></i>
                                            Client PM:
                                        </div>
                                        <div className="filter--dd">
                                            <UncontrolledDropdown>
                                            <DropdownToggle nav>
                                                <div>
                                                { this.state.filtersClientPMId != -1 ? this.state.clientPMsList[this.state.filtersClientPMId].name : 'Select' }
                                                <i className="la la-angle-down"></i>
                                                </div>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                this.state.clientPMsList.map((clientPM, index) => {
                                                    if(!clientPM.disabled)
                                                    return (
                                                    <div key={'client-pm-' + index} onClick={() => this.filtersChanged("filtersClientPMId", index)}>
                                                        <DropdownItem>{clientPM.name}</DropdownItem>
                                                    </div>
                                                    )
                                                })
                                                }
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-magic"></i>
                                            Type:
                                        </div>
                                        <div className="filter--dd">
                                            <UncontrolledDropdown>
                                            <DropdownToggle nav>
                                                <div>
                                                { this.state.filtersProjectTypeId != -1 ? this.state.projectTypesList[this.state.filtersProjectTypeId].name : 'Select' }
                                                <i className="la la-angle-down"></i>
                                                </div>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                this.state.projectTypesList.map((type, index) => {
                                                    return (
                                                    <div key={'type-' + index} onClick={() => this.filtersChanged("filtersProjectTypeId", index)}>
                                                        <DropdownItem>{type.name}</DropdownItem>
                                                    </div>
                                                    )
                                                })
                                                }
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                }
                                {this.state.userRole == 2 &&
                                    <div className="filter">
                                        <div className="filter--label">
                                            <i className="la la-male"></i>
                                            PM:
                                        </div>
                                        <div className="filter--dd">
                                            <UncontrolledDropdown>
                                            <DropdownToggle nav>
                                                <div>
                                                { this.state.filtersIdgPMId != -1 ? this.state.idgPMsList[this.state.filtersIdgPMId].name : 'Select' }
                                                <i className="la la-angle-down"></i>
                                                </div>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                this.state.idgPMsList.map((pm, index) => {
                                                    if(!pm.disabled)
                                                    return (
                                                    <div key={'idg-pm-' + index} onClick={() => this.filtersChanged("filtersIdgPMId", index)}>
                                                        <DropdownItem>{pm.name}</DropdownItem>
                                                    </div>
                                                    )
                                                })
                                                }
                                            </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </div>
                                    </div>
                                }
                            </div>

                            {this.state.userRole == 2 &&
                                <button onClick={(e) => {this.openModal()}} className="btn-new-container">
                                    <i className="la la-plus-circle"></i>
                                    Add Project
                                    {this.state.showAddProject &&
                                        <AddProject
                                            projectId={this.state.editProjectId}
                                            closeEditModal={this.closeModal}
                                            showPopup={this.state.showAddProject}
                                        />
                                    }
                                </button>
                            }

                        </div>
                        
                        <div className="TableMainContainer">
                            {
                                this.state.projectList.map((item, index) => {
                                    return (
                                        <TableRow
                                                key={'row-' + index}
                                                tableCols={
                                                    [
                                                        {
                                                            value: item.title,
                                                            label: 'Project title',
                                                            link: '/project/' + item.id,
                                                            mobileFullWidth: true,
                                                            customCssClass: 'row-details row-head-mobile'
                                                        },
                                                        {
                                                            value: item.job_number,
                                                            label: 'Job number',
                                                            customCssClass: 'row-details'
                                                        },
                                                        this.state.userRole == 2 ? {
                                                            value: item.client_name,
                                                            label: 'Client',
                                                            hiddenMobile: true,
                                                            customCssClass: 'row-details'
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            value: item.brand_name,
                                                            label: 'Brand',
                                                            hiddenMobile: true
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            value: item.hg_pm_name,
                                                            label: 'Client PM',
                                                            hiddenMobile: true,
                                                            customCssClass: 'row-details'
                                                        } : {},
                                                        {
                                                            value: item.idg_pm_name,
                                                            label: 'IDG PM',
                                                            hiddenMobile: true,
                                                            customCssClass: 'row-details'
                                                        },
                                                        this.state.userRole == 2 ? {
                                                            value: item.start_date,
                                                            dateFormat: 'DD.MM.YYYY', //momentjs formats
                                                            label: 'Start',
                                                            hiddenMobile: true
                                                        } : {},
                                                        {
                                                            value: item.deadline,
                                                            dateFormat: 'DD.MM.YYYY', //momentjs formats
                                                            label: 'Deadline',
                                                            hiddenMobile: true
                                                        },
                                                        this.state.userRole == 2 ? {
                                                            value: item.type,
                                                            label: 'Type',
                                                            hiddenMobile: true
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            value: item.estimated_cost,
                                                            label: 'Est',
                                                            hiddenMobile: true
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            value: item.actual_cost,
                                                            label: 'Act',
                                                            hiddenMobile: true
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            value: item.diff_cost,
                                                            label: 'Profit',
                                                            hiddenMobile: true,
                                                            customCssClass: item.diff_cost < 0 ? 'danger' : ''
                                                        } : {},
                                                        this.state.userRole == 2 ? {
                                                            ddOptions: [
                                                                {
                                                                    value: 'Edit this project',
                                                                    action: {
                                                                        name: 'editProject',
                                                                        params: item.id
                                                                    }
                                                                }
                                                            ]
                                                        } : {}
                                                    ]
                                                }
                                                editProject={this.editProject}
                                            /> 
                                    )
                                })
                            }
                        </div>
                        <Paginate key={this.state.paginationKey} triggerPageChange={this.pageChanged} pagesCounter={Math.ceil(this.state.totalResults / this.state.resultsPerPage)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Projects));
