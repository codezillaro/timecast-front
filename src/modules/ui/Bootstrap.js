import React, { Component } from 'react';
import AddProject from "../../components/addProject/AddProject";
import KanbanDeskTask from "../../components/kanbanDeskTask/KanbanDeskTask";
import Event from "../../components/event/Event";
import './Bootstrap.scss';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container,
    Row,
    Col,
    Jumbotron,
    Button
} from 'reactstrap';
import AddPopup from '../../components/addPopup/AddPopup';

class Bootstrap extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar color="inverse" light expand="md">
                    <NavbarBrand href="/">reactstrap</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/components/">Components</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://github.com/reactstrap/reactstrap">Github</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                <Jumbotron>
                    <Container>
                        <Row>
                            <Col>
                                <h1>Welcome to React</h1>



                                <Button
                                    tag="a"
                                    color="success"
                                    size="large"
                                    href="http://reactstrap.github.io"
                                    target="_blank"
                                >
                                    View Reactstrap Docs
                                    </Button>

                                {/* <div class="rectangle primaryBlue"><p class="codrin-test">Primary / #4D7CFE</p></div>
                                    <div class="rectangle primaryDark"><p class="codrin-test">Primary dark / #252631</p></div>
                                    <div class="rectangle secondaryDark"><p class="codrin-test">Secondary dark / #98A9BC</p></div>
                                    <div class="rectangle secondaryDark2"><p class="codrin-test">Secondary dark 2 / #98A9BC</p></div>
                                    <div class="rectangle orange"><p class="codrin-test">Secondary / #FFAB2B</p></div>
                                    <div class="rectangle green"><p class="codrin-test">Secondary / #6DD230</p></div>
                                    <div class="rectangle pink"><p class="codrin-test">Secondary / #FE4D97</p></div>
                                    <div class="rectangle secondaryBlue"><p class="codrin-test">Secondary / #2CE5F6</p></div>
                                    <div class="rectangle outline"><p class="codrin-test-dark">Outline / #E8ECEF</p></div>
                                    <div class="rectangle background"><p class="codrin-test-dark">Background / #F2F4F6</p></div>
                                    <div class="rectangle background2"><p class="codrin-test-dark">Background 2 / #F8FAFB</p></div>

                                    <button type="button" class="btn--ts btn--ts-primary">Pr Button</button>
                                    <button type="button" class="btn--ts btn--ts-outline">Out Button</button>

                                    <button type="button" class="btn--ts btn--ts-primary"><i class="la la-heart-o"></i>Pr buttons icon</button>
                                    <button type="button" class="btn--ts btn--ts-outline"><i class="la la-heart-o"></i>Out buttons icon</button> */}


                                <div style={{ marginTop: 1 + 'em' }}></div>
                                <AddProject />
                                <div style={{ marginTop: 1 + 'em' }}></div>
                                <AddPopup />

                                <div style={{ marginTop: 1 + 'em' }}>
                                    <KanbanDeskTask />
                                </div>

                                <div style={{ marginTop: 1 + 'em' }}>
                                    <Event />
                                </div>

                            </Col>

                        </Row>

                    </Container>

                    {/* <label className="chk-container">One
                        <input type="checkbox" />
                        <span className="checkmark"></span>
                    </label> */}
                </Jumbotron>
            </div>
        );
    }
}

export default Bootstrap;