import { TOGGLE_REDIRECT_TO_LOGIN } from "../constants/action-types";

const initialState = {
  redirectToLogin: false
};

function rootReducer(state = initialState, action) {
  if (action.type === TOGGLE_REDIRECT_TO_LOGIN) {
    return Object.assign({}, state, {
      redirectToLogin: action.payload
    });
  }
  return state;
}

export default rootReducer;