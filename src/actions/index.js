import { TOGGLE_REDIRECT_TO_LOGIN } from "../constants/action-types";

export function toggleRedirectToLogin(payload) {
  return { type: TOGGLE_REDIRECT_TO_LOGIN, payload }
};