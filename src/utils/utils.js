import axios from 'axios';

export const createAction = (actionTypeId, entity, entityId, token, targetedUserId) => {
    axios({
        url: process.env.REACT_APP_API_URL + 'actions',
        method: "POST",
        data: {
            action_type_id: actionTypeId,
            entity: entity,
            entity_id: entityId,
            targeted_user_id: targetedUserId || ''
        },
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
}